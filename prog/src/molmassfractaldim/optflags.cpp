#include "optflags.h"
#include "screenutils.h"
#include <iostream>
using std::cout;
using std::endl;
using std::ios;
#include <cstdlib>
using namespace std;
#include <fstream>
using std::ifstream;
#include <string>
using std::string;

OptionFlags::OptionFlags() : OptionFlagsBase() {
   minArgs=2; //Change if needed
   setnr=0;
   setnav=0;
   setmrmin=0;
   setmrmax=0;
   quiet=false;
}
OptionFlags::OptionFlags(int &argc,char** &argv) : OptionFlags() {
   /* Remember to initialize local short ints before calling Init()!  */
   Init(argc,argv);
}
OptionFlags::~OptionFlags() {
   
}
void OptionFlags::Init(int &argc, char** &argv) {
   if (!BaseProcessOptions(argc,argv)) {return;}
   GetProgramNames(argv[0]);
   if (argc>1 && string(argv[1])==string("-h")) {
      PrintHelpMenu(argc,argv);
      exitcode=OptionFlagsBase::ExitCode::OFEC_EXITNOERR;
      return;
   }
   if (argc<minArgs) {
      ScreenUtils::SetScrRedBoldFont();
      cout << "\nError: Not enough arguments." << '\n';
      ScreenUtils::SetScrNormalFont();
      cout << "\nTry: \n\t" << argv[0] << " -h\n" << '\n' << "to view the help menu.\n\n";
      exitcode=OptionFlagsBase::ExitCode::OFEC_EXITERR;
      return;
   }
   //outFileName and inFileName are processed in ProcessBaseOptions
   //cases h an V are also processed there.
   //See OptionFlagsBase class ProcessBaseOptions
   for (int i=1; i<argc; i++){
      if (argv[i][0] == '-'){
         switch (argv[i][1]){
            case 'a' :
               setnav=(++i);
               if ( i>=argc ) { PrintErrorMessage(argv,'a'); }
               break;
            case 'h' :
               PrintHelpMenu(argc,argv);
               exitcode=OptionFlagsBase::ExitCode::OFEC_EXITNOERR;
               break;
            case 'n' :
               setnr=(++i);
               if ( i>=argc ) { PrintErrorMessage(argv,'n'); }
               break;
            case 'q' :
               quiet=true;
               break;
            case 'r' :
               setmrmin=(++i);
               if ( i>=argc ) { PrintErrorMessage(argv,'r'); }
               break;
            case 'R' :
               setmrmax=(++i);
               if ( i>=argc ) { PrintErrorMessage(argv,'R'); }
               break;
            case '-':
               BaseProcessDoubleDashOption(argc,argv,i);
               ProcessDoubleDashOption(argc,argv,i);
               break;
            default:
               cout << "\nCommand line error. Unknown switch: " << argv[i] << '\n';
               cout << "\nTry: \n\t" << argv[0] << " -h\n" << '\n' << "to view the help menu.\n\n";
               exitcode=OptionFlagsBase::ExitCode::OFEC_EXITERR;
         }
      }
   }
   return;
}
void OptionFlags::PrintHelpMenu(int argc, char** &argv) {
   ScreenUtils::PrintScrStarLine();
   cout << '\n';
   ScreenUtils::CenterString(smileyprogramname);
   cout << '\n';
   ScreenUtils::CenterString("This program generates a table of N and M vs r of a protein.");
   ScreenUtils::CenterString("r is the radius of a sphere.");
   ScreenUtils::CenterString("N is the average number of particles containted");
   ScreenUtils::CenterString("within the sphere of radius r.");
   ScreenUtils::CenterString("M is the average mass of the particles contained");
   ScreenUtils::CenterString("within the sphere of radius r.");
   ScreenUtils::CenterString("In the current version the program takes as input");
   ScreenUtils::CenterString("a pdb file, which contains the structure of a protein.");
   cout << '\n';
   ScreenUtils::CenterString((string("Compilation date: ")+string(__DATE__)));
   cout << '\n';
   ScreenUtils::CenterString(string("Version: ")+string(CURRENTVERSION));
   cout << '\n';
   ScreenUtils::CenterString((string(":-) Created by: ")+string(PROGRAMCONTRIBUTORS)+string(" (-:")));
   cout << '\n';
   ScreenUtils::PrintScrStarLine();
   ScreenUtils::SetScrBoldFont();
   cout << "\nUsage:\n\n\t" << rawprogramname << " inputpdb [option [value(s)]] ... [option [value(s)]]\n\n";
   ScreenUtils::SetScrNormalFont();
   cout << "Here inputpdb is the name of the input pdb file, and options can be:\n\n";
   cout << "  -a A       \tSet the number of measures to be A (for the average N and M)." << '\n';
   cout << "  -n k       \tSet the number of radii to be k, i.e., the number of\n"
        << "             \t  rows for N(r) vs r and M(r) vs r." << '\n';
   cout << "  -q         \tUse quiet mode (most cout messages will be supressed)." << '\n';
   cout << "  -r rmn     \tSet the minimum measuring radius to be rmn. Here by radius\n"
        << "             \t  is meant the radius of the sphere)." << '\n';
   cout << "  -R rmx     \tSet the maximum measuring radius to be rmx. Here by radius\n"
        << "             \t  is meant the radius of the sphere)." << '\n';
   cout << "  -V         \tDisplays the version of this program." << '\n';
   cout << "  -h         \tDisplay the help menu.\n\n";
   //-------------------------------------------------------------------------------------
   cout << "  --help    \t\tSame as -h" << '\n';
   cout << "  --version \t\tSame as -V" << '\n';
   cout << '\n';
   ScreenUtils::PrintScrStarLine();
   //-------------------------------------------------------------------------------------
}
void OptionFlags::PrintErrorMessage(char** &argv,char lab) {
   ScreenUtils::SetScrRedBoldFont();
   string shdfll="should be followed by ";
   cout << "\nError: the option \"-" << lab << "\" ";
   switch (lab) {
      case 'r' :
      case 'R' :
         cout << shdfll << "one real number." << '\n';
         break;
      case 'o' :
         cout << shdfll << "a name." << '\n';
         break;
      case 'a' :
      case 'n' :
         cout << shdfll << "an integer." << '\n';
         break;
      default:
         cout << "is triggering an unknown error." << '\n';
         break;
   }
   ScreenUtils::SetScrNormalFont();
   cout << "\nTry:\n\t" << argv[0] << " -h " << '\n';
   cout << "\nto view the help menu.\n\n";
   exitcode=OptionFlagsBase::ExitCode::OFEC_EXITERR;
   return;
}
void OptionFlags::ProcessDoubleDashOption(int &argc,char** &argv,int &pos) {
   string str=argv[pos];
   str.erase(0,2);
   if (str==string("version")) {
      cout << rawprogramname << " " << CURRENTVERSION << '\n';
      exitcode=OptionFlagsBase::ExitCode::OFEC_EXITNOERR;
   } else if (str==string("help")) {
      PrintHelpMenu(argc,argv);
      exitcode=OptionFlagsBase::ExitCode::OFEC_EXITNOERR;
   } else {
      ScreenUtils::SetScrRedBoldFont();
      cout << "Error: Unrecognized option '" << argv[pos] << "'" << '\n';
      ScreenUtils::SetScrNormalFont();
      exitcode=OptionFlagsBase::ExitCode::OFEC_EXITERR;
   }
   return;
}
void OptionFlags::GetProgramNames(char* argv0) {
   string progname=argv0;
   size_t pos=progname.find("./");
   if (pos!=string::npos) {progname.erase(pos,2);}
   rawprogramname=progname;
   smileyprogramname=":-)  ";
   smileyprogramname+=rawprogramname;
   smileyprogramname+="  (-:";
}


