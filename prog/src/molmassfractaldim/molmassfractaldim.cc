#include <cstdlib>
#include <iostream>
using std::cout;
using std::endl;
using std::cerr;
#include <memory>
using std::shared_ptr;
#include <iomanip>
using std::scientific;
using std::setprecision;
#include <string>
using std::string;
#include <memory>
using std::shared_ptr;
#include <fstream>
using std::ifstream;
using std::ofstream;
#include <cmath>
#include "optflags.h"
#include "screenutils.h"
#include "fileutils.h"
#include "mytimer.h"
#include "massfractaldim.h"
#include "inputmolecule_pdb.h"
#include "gnuplottools.h"
#include "helpersmolmassfractaldim.h"

int main (int argc, char *argv[]) {
   /* ************************************************************************** */
   MyTimer timer;
   timer.Start();
   /* ************************************************************************** */
   /* Configures the program (options, variables, etc.)  */
   shared_ptr<OptionFlags> options = shared_ptr<OptionFlags>(new OptionFlags(argc,argv));
   switch ( options->GetExitCode() ) {
      case OptionFlagsBase::ExitCode::OFEC_EXITNOERR :
         return EXIT_SUCCESS;
         break;
      case OptionFlagsBase::ExitCode::OFEC_EXITERR :
         return EXIT_FAILURE;
         break;
      default :
         break;
   }
   string inname=string(argv[1]);
   int verboseLevel = 1;
   if ( options->verboseLevel ) {
      verboseLevel=std::stoi(string(argv[options->verboseLevel]));
   }
   if ( options->quiet ) { verboseLevel=0; }
   if ( verboseLevel > 0 ) {
      ScreenUtils::PrintHappyStart(argv,CURRENTVERSION,PROGRAMCONTRIBUTORS);
   }

   /* Main corpus  */
   // Options processing...
   int nr=50;
   if ( options->setnr ) {
      nr=std::stoi(string(argv[options->setnr]));
      if ( nr<0 ) {
         ScreenUtils::DisplayWarningMessage("Negative number of radii!");
         cout << __FILE__ << ", fnc: " << __FUNCTION__ << ", line: " << __LINE__ << '\n';
         nr=abs(nr);
      }
   }
   int nav=30;
   if ( options->setnav ) {
      nav=std::stoi(string(argv[options->setnav]));
      if ( nav<0 ) {
         ScreenUtils::DisplayWarningMessage("Negative number of measures for averages!");
         nav=abs(nav);
      }
   }
   double mRmin=0.5e0;
   if ( options->setmrmin ) {
      mRmin=std::stod(string(argv[options->setmrmin]));
      if ( mRmin<0.0e0 ) {
         ScreenUtils::DisplayWarningMessage("Negative mRmin!");
         mRmin=fabs(mRmin);
      }
   }
   double mRmax=20.0e0;
   if ( options->setmrmax ) {
      mRmax=std::stod(string(argv[options->setmrmax]));
      if ( mRmax<0.0e0 ) {
         ScreenUtils::DisplayWarningMessage("Negative mRmax!");
         mRmax=fabs(mRmax);
      }
   }
   shared_ptr<InputMoleculePDB> pdbmol=std::make_shared<InputMoleculePDB>(inname);
   shared_ptr<Molecule> mol=shared_ptr<Molecule>(pdbmol);
   MassFractalDim mfd(mol);
   mfd.SetMeasuringRmin(mRmin);
   mfd.SetMeasuringRmax(mRmax);
   mfd.SetNumberOfMeasuringCircles(nr);
   mfd.SetNumberOfCounts4Average(nav);
   mfd.ComputeNvsR();
   mfd.DisplayProperties();

   string datname=inname;
   FileUtils::ReplaceExtensionOfFileName(datname,"dat");
   FileUtils::InsertAtEndOfFileName(datname,"-Nvsr");
   cout << "Saving results to file: " << datname << '\n';
   mfd.WriteResults(datname);

   cout << "Rendering plot..." << '\n';
   HelpersMolMassFractalDim::MakePlot(datname,mfd);

   /* All OK  */
   if ( verboseLevel > 0 ) { ScreenUtils::PrintHappyEnding(); }
   timer.End();
   if ( verboseLevel > 0 ) { timer.PrintElapsedTimeSec(string("global timer")); }
   //         1         2         3         4         5         6         7         8
   //12345678901234567890123456789012345678901234567890123456789012345678901234567890
   //ATOM      1  N   GLY     1     188.560 239.250  94.670  1.00  0.00
   /* ************************************************************************** */
   return EXIT_SUCCESS;
}

