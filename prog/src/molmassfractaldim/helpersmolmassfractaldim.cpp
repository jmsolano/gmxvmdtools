#include <cstdlib>
#include <iostream>
using std::cout;
#include <fstream>
using std::ofstream;
#include <cmath>
#include "helpersmolmassfractaldim.h"
#include "screenutils.h"
#include "stringtools.h"
#include "fileutils.h"

bool HelpersMolMassFractalDim::MakePlot(const string &datname,const MassFractalDim &mfd) {
   string gnpname=datname;
   FileUtils::ReplaceExtensionOfFileName(gnpname,"gnp");
   string pdfname=datname;
   FileUtils::ReplaceExtensionOfFileName(pdfname,"pdf");
   ofstream ofil(gnpname);
   if ( !(ofil.good()) ) {
      ScreenUtils::DisplayErrorFileNotOpen(gnpname);
      cout << __FILE__ << ", fnc: " << __FUNCTION__ << ", line: " << __LINE__ << '\n';
      ofil.close();
      return false;
   }
   double maxy=(1.2e0*(mfd.M[mfd.GetNr()-1]));
   if ( (1.2e0*(mfd.N[mfd.GetNr()-1])) > maxy ) {
      maxy=(1.2e0*(mfd.N[mfd.GetNr()-1]));
   }
   ofil << "#" << '\n';
   ofil << "datname='" << datname << "'\n";
   ofil << "set xrange[1:" << (1.2e0*(mfd.r[mfd.GetNr()-1])) << ']' << '\n';
   ofil << "set yrange[1:" << maxy << ']' << '\n';
   ofil << "set xlabel 'log_{10}(r)'" << '\n';
   ofil << "set ylabel 'log_{10}(N)'" << '\n';
   ofil << "set logscale x; set format x \"10^{%T}\"" << '\n';
   ofil << "set logscale y; set format y \"10^{%T}\"" << '\n';
   ofil << "set title '" << StringTools::GetEnhancedEpsTitle(datname) << "'\n";
   ofil << "set key top left spacing 1.5 reverse" << '\n';
   ofil << "#======================================================" << '\n';
   ofil << "set style line 1 lc rgb \"black\" pointtype 1\n"
        << "set style line 2 lc rgb \"royalblue\" pointtype 2\n"
        << "set style line 3 lc rgb \"coral\" pointtype 3\n"
        << "set style line 4 lc rgb \"#228B22\" pointtype 4 #forestgreen\n"
        << "set style line 5 lc rgb \"#0000CD\" pointtype 5\n"
        << "set style line 6 lc rgb \"red\" pointtype 6\n"
        << "set style line 7 lc rgb \"green\" pointtype 7 #mediumblue" << '\n';
   ofil << "set terminal postscript eps color enhanced dashlength 4 fontscale 1.75"
        << " linewidth 2.0\n";
   ofil << "set tics nomirror in scale 2" << '\n';
   ofil << "set output '| epstopdf --filter --outfile=" << pdfname << "' \n";
   ofil << "plot \\\n"
        << "datname u 1:2 w lp ls 1 lw 2 pt 7 ps 0.75 title 'N_{av}',\\\n"
        << "datname u 1:3 w lp ls 2 lw 2 pt 7 ps 0.75 title 'M_{av}',\\\n"
        << "datname u 1:4 w lp ls 3 lw 2 pt 8 ps 0.75 title 'E_{av}'" << '\n';
   ofil.close();
   string l;
   l="gnuplot "+gnpname;
   system(l.c_str());
   //l="rm -f "+gnpname;
   //system(l.c_str());
   return true;
}

