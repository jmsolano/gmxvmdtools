#ifndef _HELPERSMASSFRACTALDIM_H_
#define _HELPERSMASSFRACTALDIM_H_
#include <vector>
using std::vector;
#include <string>
using std::string;
#include "massfractaldim.h"

/* ************************************************************************** */
class HelpersMolMassFractalDim {
/* ************************************************************************** */
public:
   static bool MakePlot(const string &datname,const MassFractalDim &mfd);
/* ************************************************************************** */
protected:
/* ************************************************************************** */
};
/* ************************************************************************** */


#endif  /* _HELPERSMASSFRACTALDIM_H_ */


