# PME potential (kT/e, T=300K)
object 1 class gridpositions counts 48 48 48
origin 105.615 102.968 55.9486
delta 3.56917 0 0
delta 0 3.49688 0
delta 0 0 5.53729
object 2 class gridconnections counts 48 48 48
object 3 class array type double rank 0 items 110592 data follows
242.729 241.21 236.88
229.79 220.022 207.688
-12.799 12.6421 38.5139
64.2959 89.5061 113.713
136.546 157.691 176.889
193.927 208.625 220.835
230.433 237.322 241.43
attribute "dep" string "positions"
object "PME potential (kT/e, T=300K)" class field
component "positions" value 1
component "connections" value 2
component "data" value 3
