#include "optflags.h"
#include "screenutils.h"
#include <iostream>
using std::cout;
using std::endl;
using std::ios;
#include <cstdlib>
using namespace std;
#include <fstream>
using std::ifstream;
#include <string>
using std::string;

OptionFlags::OptionFlags() : OptionFlagsBase() {
   minArgs=2; //Change if needed
   adddx=addscalar=scale=substractdx=substractscalar=0;
   power=0;
   convert=0;
   overwrite=sqrroot=zeros=usedoubprec=false;
   quiet=false;
}
OptionFlags::OptionFlags(int &argc,char** &argv) : OptionFlags() {
   /* Remember to initialize local short ints before calling Init()!  */
   Init(argc,argv);
}
OptionFlags::~OptionFlags() {
   
}
void OptionFlags::Init(int &argc, char** &argv) {
   if (!BaseProcessOptions(argc,argv)) {return;}
   GetProgramNames(argv[0]);
   if (argc>1 && string(argv[1])==string("-h")) {
      PrintHelpMenu(argc,argv);
      exitcode=OptionFlagsBase::ExitCode::OFEC_EXITNOERR;
      return;
   }
   if (argc<minArgs) {
      ScreenUtils::SetScrRedBoldFont();
      cout << "\nError: Not enough arguments." << '\n';
      ScreenUtils::SetScrNormalFont();
      cout << "\nTry: \n\t" << argv[0] << " -h\n" << '\n' << "to view the help menu.\n\n";
      exitcode=OptionFlagsBase::ExitCode::OFEC_EXITERR;
      return;
   }
   //outFileName and inFileName are processed in ProcessBaseOptions
   //cases h an V are also processed there.
   //See OptionFlagsBase class ProcessBaseOptions
   for (int i=1; i<argc; i++){
      if (argv[i][0] == '-'){
         switch (argv[i][1]){
            case 'a' :
               addscalar=(++i);
               if (i>=argc) { PrintErrorMessage(argv,'a'); }
               break;
            case 'A' :
               adddx=(++i);
               if (i>=argc) { PrintErrorMessage(argv,'A'); }
               break;
            case 'c' :
               convert=(++i);
               if (i>=argc) { PrintErrorMessage(argv,'c'); }
               break;
            case 'd' :
               usedoubprec=true;
               break;
            case 'm' :
               scale=(++i);
               if (i>=argc) { PrintErrorMessage(argv,'m'); }
               break;
            case 'O' :
               overwrite=true;
               break;
            case 'p' :
               power=(++i);
               if (i>=argc) { PrintErrorMessage(argv,'p'); }
               break;
            case 'q' :
               quiet=true;
               break;
            case 'r' :
               sqrroot=true;
               break;
            case 's' :
               substractscalar=(++i);
               if (i>=argc) { PrintErrorMessage(argv,'s'); }
               break;
            case 'S' :
               substractdx=(++i);
               if (i>=argc) { PrintErrorMessage(argv,'S'); }
               break;
            case 'z' :
               zeros=true;
               break;
            case '-':
               BaseProcessDoubleDashOption(argc,argv,i);
               ProcessDoubleDashOption(argc,argv,i);
               break;
            default:
               cout << "\nCommand line error. Unknown switch: " << argv[i] << '\n';
               cout << "\nTry: \n\t" << argv[0] << " -h\n" << '\n' << "to view the help menu.\n\n";
               exitcode=OptionFlagsBase::ExitCode::OFEC_EXITERR;
         }
      }
   }
   return;
}
void OptionFlags::PrintHelpMenu(int argc, char** &argv) {
   ScreenUtils::PrintScrStarLine();
   cout << '\n';
   ScreenUtils::CenterString(smileyprogramname);
   cout << '\n';
   ScreenUtils::CenterString("This program manipulates dx files. A manipulation");
   ScreenUtils::CenterString("consists of performing a single operation over a dx file.");
   ScreenUtils::CenterString("The operation can be one of the following:");
   ScreenUtils::CenterString("adding (substracting) another dx, adding");
   ScreenUtils::CenterString("(substracting) a scalar, multiplying by");
   ScreenUtils::CenterString("an scalar (i.e., scaling), etc.");
   cout << '\n';
   ScreenUtils::CenterString((string("Compilation date: ")+string(__DATE__)));
   cout << '\n';
   ScreenUtils::CenterString(string("Version: ")+string(CURRENTVERSION));
   cout << '\n';
   ScreenUtils::CenterString((string(":-) Created by: ")+string(PROGRAMCONTRIBUTORS)+string(" (-:")));
   cout << '\n';
   ScreenUtils::PrintScrStarLine();
   ScreenUtils::SetScrBoldFont();
   cout << "\nUsage:\n\n\t" << rawprogramname << "inputdx [option [value(s)]] ... [option [value(s)]]\n\n";
   ScreenUtils::SetScrNormalFont();
   cout << "Here inputdx is the name of the input dx file, and options can be:\n\n";
   cout << "  -a x       \tAdd x to each point of the field contained in the inputdx file." << '\n';
   cout << "  -A otherdx \tAdd the field otherdx to the inputdx." << '\n';
   cout << "  -c fmt     \tConvert the dx to the format fmt. fmt can be:\n"
        << "             \t\ttsv (tab separated values)\n"
        << "             \t\tdat4c (data 4 columns: x,y,z,prop), where prop is the\n"
        << "             \t\t  property contained in the dx file.\n";
   cout << "  -d         \tUse double precision while writing the output dx file.\n"
        << "             \t  This will not affect the input files, but output files may cause\n"
        << "             \t  issues with other packages not developed in LabMathModSSD."<< '\n';
   cout << "  -m c       \tMultiply the field times the constant c." << '\n';
   cout << "  -o outfname\tSets the output file name to be outfname (default: output.dx)." << '\n';
   cout << "  -q         \tUse quiet mode (most cout messages will be supressed)." << '\n';
   cout << "  -s x       \tSubstract x to each point of the field contained in the inputdx\n"
        << "             \t  file." << '\n';
   cout << "  -S otherdx \tSubstract the field otherdx to the inputdx (inputdx-otherdx)." << '\n';
   cout << "  -O         \tOverwrite the inputdx (the contents of the original dx will be\n"
        << "             \t  overwritten).\n"
        << "             \t  This option overrides option -o." << '\n';
   cout << "  -p n       \tRaise each point, p[i], of the field to the n power (i.e.,\n"
        << "             \t  p[i] -> (p[i])^n." << '\n';
   cout << "  -r         \tSquare-root each point, i.e., p[i] -> sqrt(p[i])." << '\n';
   cout << "  -z         \tSet to zero every point." << '\n';
   cout << "  -V         \tDisplays the version of this program." << '\n';
   cout << "  -h         \tDisplay the help menu.\n\n";
   //-------------------------------------------------------------------------------------
   cout << "  --add-pow-other otherdx n\n"
        << "             \tAdd otherdx data points raised to the nth power to the inputdx." << '\n';
   cout << "  --help    \t\tSame as -h" << '\n';
   cout << "  --version \t\tSame as -V" << '\n';
   cout << '\n';
   ScreenUtils::PrintScrStarLine();
   //-------------------------------------------------------------------------------------
}
void OptionFlags::PrintErrorMessage(char** &argv,char lab) {
   ScreenUtils::SetScrRedBoldFont();
   string shdfll="should be followed by ";
   cout << "\nError: the option \"-" << lab << "\" ";
   switch (lab) {
      case 'a' :
      case 'm' :
      case 's' :
         cout << shdfll << "one real number." << '\n';
         break;
      case 'A' :
      case 'o' :
      case 'S' :
         cout << shdfll << "a name." << '\n';
         break;
      case 'p' :
         cout << shdfll << "an integer." << '\n';
         break;
      case 'c' :
         cout << shdfll << "a string." << '\n';
         break;
      default:
         cout << "is triggering an unknown error." << '\n';
         break;
   }
   ScreenUtils::SetScrNormalFont();
   cout << "\nTry:\n\t" << argv[0] << " -h " << '\n';
   cout << "\nto view the help menu.\n\n";
   exitcode=OptionFlagsBase::ExitCode::OFEC_EXITERR;
   return;
}
void OptionFlags::ProcessDoubleDashOption(int &argc,char** &argv,int &pos) {
   string str=argv[pos];
   str.erase(0,2);
   if (str==string("version")) {
      cout << rawprogramname << " " << CURRENTVERSION << '\n';
      exitcode=OptionFlagsBase::ExitCode::OFEC_EXITNOERR;
   } else if (str==string("help")) {
      PrintHelpMenu(argc,argv);
      exitcode=OptionFlagsBase::ExitCode::OFEC_EXITNOERR;
   } else if (str==string("add-pow-other")) {
      addpowoth=(++pos);
      ++pos;
      if (pos>=argc) {
         ScreenUtils::DisplayErrorMessage(string("Option --")+str+
               string(" should be followed by a name and an integer."));
         exitcode=OptionFlagsBase::ExitCode::OFEC_EXITERR;
      }
   } else {
      ScreenUtils::SetScrRedBoldFont();
      cout << "Error: Unrecognized option '" << argv[pos] << "'" << '\n';
      ScreenUtils::SetScrNormalFont();
      exitcode=OptionFlagsBase::ExitCode::OFEC_EXITERR;
   }
   return;
}
void OptionFlags::GetProgramNames(char* argv0) {
   string progname=argv0;
   size_t pos=progname.find("./");
   if (pos!=string::npos) {progname.erase(pos,2);}
   rawprogramname=progname;
   smileyprogramname=":-)  ";
   smileyprogramname+=rawprogramname;
   smileyprogramname+="  (-:";

}


