#include <cstdlib>
#include <iostream>
using std::cout;
using std::endl;
using std::cerr;
#include <memory>
using std::shared_ptr;
#include <iomanip>
using std::scientific;
using std::setprecision;
#include <string>
using std::string;
#include <memory>
using std::shared_ptr;
#include <fstream>
using std::ifstream;
using std::ofstream;
#include "optflags.h"
#include "screenutils.h"
#include "fileutils.h"
#include "mytimer.h"
#include "opendxvolumetricmap.h"

int main (int argc, char *argv[]) {
   /* ************************************************************************** */
   MyTimer timer;
   timer.Start();
   /* ************************************************************************** */
   /* Configures the program (options, variables, etc.)  */
   shared_ptr<OptionFlags> options = shared_ptr<OptionFlags>(new OptionFlags(argc,argv));
   switch ( options->GetExitCode() ) {
      case OptionFlagsBase::ExitCode::OFEC_EXITNOERR :
         std::exit(EXIT_SUCCESS);
         break;
      case OptionFlagsBase::ExitCode::OFEC_EXITERR :
         std::exit(EXIT_FAILURE);
         break;
      default :
         break;
   }
   string inname=string(argv[1]);
   string outname="output.dx";
   int verboseLevel = 1;
   if ( options->verboseLevel ) {
      verboseLevel=std::stoi(string(argv[options->verboseLevel]));
   }
   if ( options->quiet ) { verboseLevel=0; }
   if ( verboseLevel > 0 ) {
      ScreenUtils::PrintHappyStart(argv,CURRENTVERSION,PROGRAMCONTRIBUTORS);
   }

   /* Main corpus  */
   OpenDxVolumetricMap map(inname);

   if ( options->scale ) {
      double f=std::stod(string(argv[options->scale]));
      map.Scale(f);
   } else if ( options->addscalar ) {
      double a=std::stod(string(argv[options->addscalar]));
      map.Add(a);
   } else if ( options->substractscalar ) {
      double a=std::stod(string(argv[options->substractscalar]));
      map.Substract(a);
   } else if ( options->adddx ) {
      string othername=string(argv[options->adddx]);
      OpenDxVolumetricMap m2(othername);
      map.Add(m2);
   } else if ( options->substractdx ) {
      string othername=string(argv[options->substractdx]);
      OpenDxVolumetricMap m2(othername);
      map.Substract(m2);
   } else if ( options->power ) {
      int k=std::stoi(string(argv[options->power]));
      map.Power(k);
   } else if ( options->addpowoth ) {
      string othername=string(argv[options->addpowoth]);
      int k=std::stoi(string(argv[options->addpowoth+1]));
      OpenDxVolumetricMap m2(othername);
      map.AddPowerOfOther(m2,k);
   } else if ( options->sqrroot ) {
      map.SqrRoot();
   } else if ( options->zeros ) {
      map.Zeros();
   }

   if ( options->overwrite ) { outname=inname; }
   if ( options->convert ) {
      outname=inname;
      string fmt=string(argv[options->convert]);
      if ( fmt==string("tsv") ) {
         FileUtils::ReplaceExtensionOfFileName(outname,"tsv");
         map.ExportAsDataFile(outname,fmt);
      } else if ( fmt == string("dat4c") ) {
         FileUtils::ReplaceExtensionOfFileName(outname,"dat");
         map.ExportAsDataFile(outname,fmt);
      } else {
         string msg=string("The format '")+fmt\
                    +string("' is not supported in this version!\n");
         msg+="Nothing done!";
         ScreenUtils::DisplayErrorMessage(msg);
         cout << __FILE__ << ", line: " << __LINE__ << '\n';
      }
   } else {
      map.Write(outname,options->usedoubprec);
   }
   /* All OK  */
   if ( verboseLevel > 0 ) { ScreenUtils::PrintHappyEnding(); }
   timer.End();
   if ( verboseLevel > 0 ) { timer.PrintElapsedTimeSec(string("global timer")); }
   //         1         2         3         4         5         6         7         8
   //12345678901234567890123456789012345678901234567890123456789012345678901234567890
   //ATOM      1  N   GLY     1     188.560 239.250  94.670  1.00  0.00
   /* ************************************************************************** */
   return EXIT_SUCCESS;
}

