#ifndef _HELPERSFINDCHAPCAVSHAPE_H_
#define _HELPERSFINDCHAPCAVSHAPE_H_
#include <memory>
using std::shared_ptr;
#include "molecule.h"
#include "moleculeinertiatensor.h"
#include "symmetricsurfacegrid.h"
#include "povraytools.h"

/* ************************************************************************** */
class HelpersFindChapCavShape {
/* ************************************************************************** */
public:
/* ************************************************************************** */
   static void PrintDebugInformation(shared_ptr<Molecule> &m,\
         shared_ptr<MoleculeInertiaTensor> &I);
   static shared_ptr<POVRayConfiguration> GetPOVRayConf(shared_ptr<SymmetricSurfaceGrid> &g);
/* ************************************************************************** */
protected:
/* ************************************************************************** */
/* ************************************************************************** */
};
/* ************************************************************************** */


#endif  /* _HELPERSFINDCHAPCAVSHAPE_H_ */

