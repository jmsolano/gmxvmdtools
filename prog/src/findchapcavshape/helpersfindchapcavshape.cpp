#include <cstdlib>
#include <iostream>
using std::cout;
#include <iomanip>
using std::scientific;
using std::setprecision;
#include "matrixvectoroperations3d.h"
#include "helpersfindchapcavshape.h"

void HelpersFindChapCavShape::PrintDebugInformation(shared_ptr<Molecule> &m,\
      shared_ptr<MoleculeInertiaTensor> &I) {
   cout << scientific << setprecision(10) << "Center of mass: ";
   cout << m->cm[0] << ' ' << m->cm[1] << ' ' << m->cm[2] << '\n';
   cout << "      Centroid: ";
   cout << m->cd[0] << ' ' << m->cd[1] << ' ' << m->cd[2] << '\n';
   cout << "  Bounding box: ";
   cout << m->xmin[0] << ' ' << m->xmin[1] << ' ' << m->xmin[2] << '\n';
   cout << "                ";
   cout << m->xmax[0] << ' ' << m->xmax[1] << ' ' << m->xmax[2] << '\n';
   I->DisplayEigenvalues();
   I->DisplayEigenvectors();
}
shared_ptr<POVRayConfiguration> HelpersFindChapCavShape::GetPOVRayConf(shared_ptr<SymmetricSurfaceGrid> &g) {
   shared_ptr<POVRayConfiguration> pc= shared_ptr<POVRayConfiguration>(new POVRayConfiguration());
   g->DetermineSurroundingBox();
   double rview=-1.0e+50;
   double tmp=MatrixVectorOperations3D::Norm(g->xmin);
   if ( tmp > rview ) { rview=tmp; }
   tmp=MatrixVectorOperations3D::Norm(g->xmax);
   if ( tmp > rview ) { rview=tmp; }
   pc->SetupUsualLightSources(4.5*rview);
   pc->SelectStandardCameraVectors(4.0*rview);
   return pc;
}
