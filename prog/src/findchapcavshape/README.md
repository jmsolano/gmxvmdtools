
# findchapcavshape (Find chaperone's cavity shape)

The program findchapcavshape finds the shape of the HSP60/HSP10' cavity.

The main algorithm to determine the shape is as follows.

* As input, the HSP60/HSP10 (HSP60-10 or chaperone for short) must be provided through a pdb file. In the first version of the program, this pdb file must comply with the following conditions:
   1. The chaperone's shortest principal axis of inertia is assumed to be aligned along the z-axis.
   2. The pdb has only one frame. If it has more than one frame, findchapcavshape will read only the first frame, ignoring the rest.
* A polygonized  sphere will be added approximately in the center of the top cavity (along the z-axis).
* The vertices of the sphere will be *grown* by means of an ideal gas expansion. At each time-step, the area of each triangle will be computed, and if a certain threshold is crossed, then the triangle is subdivided into four new triangles.
* The growth will be stopped if the any of the triangles' vertices is located outside the box that contains the chaperone.

