#include <cstdlib>
#include <iostream>
using std::cout;
using std::endl;
using std::cerr;
#include <vector>
using std::vector;
#include <memory>
using std::shared_ptr;
#include <iomanip>
using std::scientific;
using std::setprecision;
#include <string>
using std::string;
#include <memory>
using std::shared_ptr;
#include <fstream>
using std::ifstream;
using std::ofstream;
#include "optflags.h"
#include "screenutils.h"
#include "fileutils.h"
#include "mytimer.h"
#include "inputmolecule_pdb.h"
#include "moleculegeometricoperations.h"
#include "moleculeinertiatensor.h"
#include "symmetricsurfacegrid.h"
#include "helpersfindchapcavshape.h"
#include "povraytools.h"
#include "cavitygrower.h"

int main (int argc, char *argv[]) {
   /* ************************************************************************** */
   MyTimer timer;
   timer.Start();
   /* ************************************************************************** */
   /* Configures the program (options, variables, etc.)  */
   shared_ptr<OptionFlags> options = shared_ptr<OptionFlags>(new OptionFlags(argc,argv));
   switch ( options->GetExitCode() ) {
      case OptionFlagsBase::ExitCode::OFEC_EXITNOERR :
         std::exit(EXIT_SUCCESS);
         break;
      case OptionFlagsBase::ExitCode::OFEC_EXITERR :
         std::exit(EXIT_FAILURE);
         break;
      default :
         break;
   }
   string inname=string(argv[1]);
   string outname=inname;
   FileUtils::InsertAtEndOfFileName(outname,"Mod");
   ScreenUtils::PrintHappyStart(argv,CURRENTVERSION,PROGRAMCONTRIBUTORS);

   /* Main corpus  */

   int count=1;
   cout << "Reading frame " << (count++) << '\n' << std::flush;
   shared_ptr<InputMoleculePDB> pdbmol=
      shared_ptr<InputMoleculePDB>(new InputMoleculePDB());
   ifstream ifil(inname);
   size_t pos=pdbmol->ReadModel(ifil);
   if ( pos == string::npos ) {
      ScreenUtils::DisplayErrorMessage(string("Could not read frame!"));
      cout << __FILE__ << ", line: " << __LINE__ << '\n';
      ifil.close();
      return EXIT_FAILURE;
   }
   ifil.close();
   shared_ptr<Molecule> mol=shared_ptr<Molecule>( pdbmol );
   shared_ptr<MoleculeInertiaTensor> inertia=
      std::make_shared<MoleculeInertiaTensor>(mol);
   if ( options->align ) {
      inertia->DisplayEigenvalues();
      inertia->DisplayEigenvectors();
      MoleculeGeometricOperations::AlignFirstAndSecondMoments(mol,inertia,false);
   }
   mol->ComputeCenterOfMass();
   mol->ComputeCentroid();
   mol->DetermineBoundingBox();

   HelpersFindChapCavShape::PrintDebugInformation(mol,inertia);
   shared_ptr<SymmetricSurfaceGrid> sph = shared_ptr<SymmetricSurfaceGrid>(new SymmetricSurfaceGrid(SymmetricSurfaceGrid::Shape::SPHEREICOSAHEDRON));
   shared_ptr<MeshGrid> mg = shared_ptr<MeshGrid> (sph);
   vector<double> t(3);
   t[0]=mol->xmin[0]+0.75*((mol->xmax[0])-(mol->xmin[0]));
   t[1]=t[2]=0.0e0;
   //sph->TranslateBase(t);

   CavityGrower cg(sph,mol);
   cg.ExpandToACube(3.5e0,10045);
   sph->DetermineEdges();

   /*
   for ( size_t i=0 ; i<sph->vertex.size() ; ++i ) {
      //cout << sph->vertex[i][0] << ' ' << sph->vertex[i][1] << ' ' << sph->vertex[i][2] << '\n';
      cout << MatrixVectorOperations3D::Norm(sph->vertex[i]) << '\n';
   }
   // */
   
   shared_ptr<POVRayConfiguration> pc=HelpersFindChapCavShape::GetPOVRayConf(sph);
   string povname=inname;
   FileUtils::ReplaceExtensionOfFileName(povname,"pov");
   ofstream ofil(povname);
   pc->WriteHeader(ofil);
   cout << "g: " << (*mg).vertex.size() << '\n';
   HelpersMeshGrid::AddFaces2POVAsMesh(ofil,*mg,0.888,false,0);
   HelpersMeshGrid::AddVertices2POVAsSpheres(ofil,*mg,0.9,0.03,0);
   HelpersMeshGrid::AddEdges2POVAsCylinders(ofil,*mg,0.8,0.02,0);
   ofil.close();
   ScreenUtils::PrintHappyEnding();
   // Note for the future: using a complete line full of [0-9] causes vim to
   // deactivate syntax. It may have to do with redrawtime.
   //         1         2         3         4         5         6         7         8
   //123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
   //ATOM      1  N   GLY     1     188.560 239.250  94.670  1.00  0.00
   /* ************************************************************************** */
   timer.End();
   timer.PrintElapsedTimeSec(string("global timer"));
   return EXIT_SUCCESS;
}

