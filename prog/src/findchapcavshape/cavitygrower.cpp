#include <cstdlib>
#include <iostream>
using std::cout;
#include "cavitygrower.h"
#include "../common/matrixvectoroperations3d.h"

CavityGrower::CavityGrower() {
   m=nullptr;
   ssg=nullptr;
   g=nullptr;
   maxarea=2.0e0;
   dt=0.05;
}
CavityGrower::CavityGrower(shared_ptr<SymmetricSurfaceGrid> &ugrid,\
      shared_ptr<Molecule> &umol) : CavityGrower() {
   ssg=ugrid;
   g=shared_ptr<MeshGrid>(ssg);
   m=umol;
}
void CavityGrower::ExpandToACube(const double edlen,const size_t nsteps) {
   double maxa2=maxarea*maxarea;
   double mina2=1.0e-02;
   double triarea,dr=1.0e0;
   size_t step=0;
   double emin=-0.5e0*edlen;
   double emax=0.5e0*edlen;
   size_t nf;
   bool subdivtris,nominarea=true;
   cout << "nvpf: " << g->face[0].size() << '\n';
   cout << "f[0]: " << g->face[0][0] << ' ' << g->face[0][1] << ' ' << g->face[0][2] << '\n';
   while ( fabs(dr)>1.0e-10 && step<nsteps && nominarea ) {
      g->ComputeNormal2FaceVectors();
      nf=g->face.size();
      subdivtris=false;
      nominarea=true;
      for ( size_t i=0 ; i<nf ; ++i ) {
         triarea=MatrixVectorOperations3D::InnerProduct(g->normal[i],g->normal[i]);
         if ( triarea >= maxa2 ) { subdivtris=true; }
         if ( triarea < mina2 ) { nominarea=false; }
      }
      if ( subdivtris ) {
         ssg->SubdivideAllTriangles(1,false);
         ssg->CheckTriangleOrientation();
         g->ComputeNormal2FaceVectors();
      }
      //g->NormaliseNormals();
      dr=MoveVertices(emin,emax);
      ++step;
   }
   cout << "f[0]: " << g->face[0][0] << ' ' << g->face[0][1] << ' ' << g->face[0][2] << '\n';
   cout << "dr: " << dr << ", seps: " << step << '\n';
}
double CavityGrower::MoveVertices(const double emin,const double emax) {
   double maxh=-1.0e+50,tmp;
   size_t nvpf=g->face[0].size();
   double f[3],h[3];
   size_t pos;
   size_t nf=g->face.size();
   for ( size_t i=0 ; i<nf ; ++i ) {
      for ( size_t k=0 ; k<3 ; ++k ) { f[k]=0.0e0; }
      for ( size_t k=0 ; k<3 ; ++k ) { f[k]+=(0.5e0*dt*dt*(g->normal[i][k])); }
      for ( size_t j=0 ; j<nvpf ; ++j ) {
         pos=g->face[i][j];
         for ( size_t k=0 ; k<3 ; ++k ) {
            h[k]=g->vertex[pos][k];
            (g->vertex[pos][k]) += f[k];
            if ( g->vertex[pos][k] >= emax ) { g->vertex[pos][k]=emax; }
            if ( g->vertex[pos][k] <= emin ) { g->vertex[pos][k]=emin; }
            h[k]-=(g->vertex[pos][k]);
         }
         tmp=sqrt(h[0]*h[0]+h[1]*h[1]+h[2]*h[2]);
         if ( tmp>maxh ) { maxh=tmp; }
      }
   }
   return maxh;
}

