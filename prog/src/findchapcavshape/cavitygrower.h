#ifndef _CAVITYGROWER_H_
#define _CAVITYGROWER_H_
#include <memory>
using std::shared_ptr;
#include "molecule.h"
#include "../common/symmetricsurfacegrid.h"

/* ************************************************************************** */
class CavityGrower {
/* ************************************************************************** */
public:
/* ************************************************************************** */
   CavityGrower(shared_ptr<SymmetricSurfaceGrid> &ugrid,shared_ptr<Molecule> &umol);
/* ************************************************************************** */
   void ExpandToACube(const double edlen=10.0,const size_t nsteps=100);
   double MoveVertices(const double emin,const double emax);
/* ************************************************************************** */
   double maxarea;
   double dt;
/* ************************************************************************** */
protected:
/* ************************************************************************** */
   shared_ptr<Molecule> m;
   shared_ptr<MeshGrid> g;
   shared_ptr<SymmetricSurfaceGrid> ssg;
/* ************************************************************************** */
   CavityGrower(); // Forbids to use default constructor.
/* ************************************************************************** */
};
/* ************************************************************************** */


#endif  /* _CAVITYGROWER_H_ */

