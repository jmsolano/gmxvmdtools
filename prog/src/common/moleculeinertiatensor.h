#ifndef _MOLECULEINERTIATENSOR_H_
#define _MOLECULEINERTIATENSOR_H_
#include <memory>
using std::shared_ptr;
#include <vector>
using std::vector;
#include "molecule.h"
#include <cassert>

/* ************************************************************************** */
class MoleculeInertiaTensor {
/* ************************************************************************** */
public:
   MoleculeInertiaTensor(shared_ptr<Molecule> &umol);
   void Setup();
   void DisplayEigenvalues();
   void DisplayEigenvectors();
   void DisplayNormalI();
   void Diagonalize();
   double ComputeNormFactor();
   double TotalMass();
   void ComputeInertiaTensor();
   void CenterMolecule();
   void ComputeTotalMass();
   void ComputeNormalData();
   /** Returns the element data[row][col]  */
   double& operator()(int row, int col);
   /** Returns the element data[row][col]  */
   const double& operator()(int row, int col) const; // for const objects
   vector<double> Eve(int idx) const {vector<double> r(evec[idx]); return r;}
   vector<vector<double> > Eve() const {return evec;}
   vector<double> Eva(void) const {return eval;}
   vector<double> CenterOfMass() const {return initCentOfMass;}
   double Eva(int idx) const {assert(idx<4 && idx>=0); return eval[idx];}
   double NormFactEva() const {return normFact;}
/* ************************************************************************** */
protected:
/* ************************************************************************** */
   void Init();
   MoleculeInertiaTensor();
   vector<vector<double> > data; /*!< Holds the inertia tensor's data  */
   vector<vector<double> > normaldata; /*!< Normalized data.  */
   vector<vector<double> > evec;
   shared_ptr<Molecule> molecule;
   vector<double> initCentOfMass,eval;
   double totalMass,normFact;
/* ************************************************************************** */
};
/* ************************************************************************** */


#endif  /* _MOLECULEINERTIATENSOR_H_ */

