#ifndef _MASSFRACTALDIM_H_
#define _MASSFRACTALDIM_H_
#include <random>
#include <memory>
using std::shared_ptr;
#include <string>
using std::string;
#include "molecule.h"

/* ************************************************************************** */
/** This class produces a table of \f$\bar N_i\f$ vs \f$r_i\f$.
   Here \f$\bar N_i\f$ is the average number of
   particles contained around a circle of radius \f$r_i\f$. The average
   is taken over several circles centered at random points. */
class MassFractalDim {
/* ************************************************************************** */
public:
/* ************************************************************************** */
   MassFractalDim();
   ~MassFractalDim();
   MassFractalDim(shared_ptr<Molecule> umol);
   size_t GetNr() const { return nr; }
   double GetMRmin() const { return mrmin; }
   double GetMRmax() const { return mrmax; }
   void SetMeasuringRmin(const double urmn) { mrmin=urmn; SetupInternalVariables(); }
   void SetMeasuringRmax(const double urmx) { mrmax=urmx; SetupInternalVariables(); }
   void SetNumberOfMeasuringCircles(const int un);
   void SetNumberOfCounts4Average(const size_t unn) { nMeas=unn; } 
   void DisplayProperties();
   void DisplayResults();
   void WriteResults(const string &outfname);
   void ComputeNvsR();
   void AddSingleCount(const size_t idx);
   void IdentifyParticlesInsideMRmaxCircle(const size_t idx);
   size_t GetSeedIdx();
   bool SetupInternalVariables();
   /** Sets the initial values for each r[i] and N[i]; r[i] goes from mrmin
    * to mrmax, and N[i] are set to zero. */
   void InitVectors();
/* ************************************************************************** */
   vector<double> r; /*!< The array of circle radii */
   vector<double> N; /*!< The mean number of particles per radius. */
   vector<double> M; /*!< The approximate mass per radius. */
   vector<double> E; /*!< The approximate number of electrons per radius. */
/* ************************************************************************** */
protected:
/* ************************************************************************** */
   shared_ptr<Molecule> mol;
   vector<vector<double> > x; /*!< Particle's positions. */
   vector<double> mass; /*!< Particle's masses */
   vector<double> numel; /*!< Particle's number of electronx. */
   size_t nat; /*!< The number of particles in the molecule */
   double Rmax;/*!< The maximum radius found in the cluster/molecule/... */
   double seekRmax; /*!< The maximum radius valid to performing a measure. */
   double mrmax; /*!< The [m]easuring rmax. */
   double mrmin; /*!< The [m]easuring rmin. */
   double dr; /** The [delta]r. */
   size_t nr; /*!< The number of internal radii including mrmin and mrmax. */
   size_t nMeas; /*!< The number of measurings per center (for computing \f$\bar N\f$. */
   size_t nw; /*!< This number is used to reserve memory of pw. */
   vector<size_t> pw; /*!< Watch particles: particles inside rmax. */
   bool imsetup;
/* ************************************************************************** */
   static std::mt19937_64 generator;
   static std::uniform_int_distribution<size_t> distrib;
/* ************************************************************************** */
};
/* ************************************************************************** */


#endif  /* _MASSFRACTALDIM_H_ */

