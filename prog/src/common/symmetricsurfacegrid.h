#ifndef _SYMMETRICSURFACEGRID_H_
#define _SYMMETRICSURFACEGRID_H_
#include <vector>
using std::vector;
#include "meshgrid.h"

/* ************************************************************************** */
class SymmetricSurfaceGrid : public MeshGrid {
/* ************************************************************************** */
public:
/* ************************************************************************** */
   enum class Shape {
      NONE,//
      CUBOID,//Vertices 
      SPHERESQUARES, //A sphere whose vertices are determined through a projected cube
      SPHEREANGLES,//A sphere whose vertes are through angles theta and phi
      SPHEREICOSAHEDRON, //A sphere whose vertices are projected from an icosahedron
      CYLINDER
   };
   SymmetricSurfaceGrid();
   SymmetricSurfaceGrid(Shape sh);
   ~SymmetricSurfaceGrid();
   /** Projects the vertices of a cube into a unit sphere. If
    * it==0, then the mesh is a cube of edges equal to sqrt(2).
    * Each iteration divides all faces into 9 subfaces, i.e.,
    * each iteration divides a face into 3x3 smaller equal faces.  */
   void SetupSphereSquares(int it=0);
   /** Projects the vertices of the icosahedron onto a unit sphere. If
    * it==0, then the mesh is an icosahedron.  */
   void SetupSphereIcosahedron(const int it=0);
   void SubdivideAllSquares(int it=1);
   void SubdivideSquare(size_t idx);
   /** Divide every triangle face into 4 subtriangles.
    * \param it[in] it is the number of times the split will be performed.
    * \param normvtxcrd[in] if true, the function assigns the vertices' magnitudes to be 1,
    * otherwise the new vertices are the edges' midpoints */
   void SubdivideAllTriangles(int it=1,bool normvtxcrd=true);
   /** Split the idx-th triangle into four subtriangles.
    * \param idx[in] the index of the triangle (i.e. the
    *    face[idx] will be subdivided).
    * \param normvtxcrd[in] assign the vertex magnitude to be 1.  */
   void SubdivideTriangle(size_t idx,bool normvtxcrd=true);
   void GenerateTrianglesFromSquares();
   void AddTrianglesFromSquare(size_t idx);
   void CheckTriangleOrientation();
   /** Returns the position of t within vertex. If t is not
    * in vertex, the function appends t to vertex and returns
    * vertex.size() before insertion [=vertex.size()-1 after insertion].  */
   void TrimFacesCentroidDotProdGreaterThanZero(const vector<double> &d) {return TrimFacesCentroidDotProdBetweenVals(d,0.0e0,1.0e0); }
   void TrimFacesCentroidDotProdBetweenVals(const vector<double> &d,const double val1,const double val2);
   void RemoveFacesUsingVertices(const vector<size_t> &v2r);
   void ComputeCentroids();
   void RemoveUnusedVertices();
   void DisplayFaces();
/* ************************************************************************** */
   vector<vector<size_t> > sface;/*!< faces of squares.  */
/* ************************************************************************** */
protected:
/* ************************************************************************** */
   Shape shape;
/* ************************************************************************** */
   void ClearArrays();
/* ************************************************************************** */
};
/* ************************************************************************** */


#endif  /* _SYMMETRICSURFACEGRID_H_ */

