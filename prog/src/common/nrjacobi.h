/** This class and derived functions are a refactor of the code
   * distributed by GROMACS under the name nrjac.{h,c}.
   * Up to 2022-06-04, the refactor consists of translating the
   * original code into a C++ version, which uses std::vectors,
   * as opposed to c-style arrays.
   * The initial translation was performed by JMSA.
*/
#ifndef NRJACOBI_H
#define NRJACOBI_H
#include <vector>
using std::vector;
/* ************************************************************************** */
class NRJacobi {
/* ************************************************************************** */
public:
   /** vector<vector<double> > omega = input matrix a[0..n-1][0..n-1] must be symmetric
    * int     n    = number of rows and columns
    * vector<double> d:  d[0]..d[n-1] are the eigenvalues of a[][]
    * vector<vector<double> > evec = evec[0..n-1][0..n-1] the eigenvectors:
    *                                    evec[i][j] is component i of vector j
    * int      nrot = number of Jacobi rotations
    */
   static void Jacobi(vector<vector<double> > &a,vector<double> &eval,\
         vector<vector<double> > &evec,int &nrot);
   static void Jacobi(vector<vector<double> > &a,vector<double> &eval,\
         vector<vector<double> > &evec);
/* ************************************************************************** */
/* ************************************************************************** */
protected:
/* ************************************************************************** */
/* ************************************************************************** */
};
/* ************************************************************************** */


int m_inv_gen(double *m, int n, double *minv);
/* Produces minv, a generalized inverse of m, both stored as linear arrays.
 * Inversion is done via diagonalization,
 * eigenvalues smaller than 1e-6 times the average diagonal element
 * are assumed to be zero.
 * For zero eigenvalues 1/eigenvalue is set to zero for the inverse matrix.
 * Returns the number of zero eigenvalues.
 */

#endif
