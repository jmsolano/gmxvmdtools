#ifndef _INPUTMOLECULE_PDB_H_
#define _INPUTMOLECULE_PDB_H_
#include <string>
using std::string;
#include <fstream>
using std::ifstream;
using std::ofstream;
#include <vector>
using std::vector;
#include "molecule.h"

enum class PDBRecordType {UNDEF,ATOM,HETATM,ANISOU,CRYST1,\
   COMPND,MODEL,ENDMDL,TER,HEADER,TITLE,REMARK,CONECT};
/* ************************************************************************** */
class InputMoleculePDB : public Molecule {
/* ************************************************************************** */
public:
   InputMoleculePDB();
   InputMoleculePDB(string fname);
   InputMoleculePDB(string fname,short int setVbsLvl);
   void ReadFromFile(string fname);
   /** Reads a single model from the pdb file. The reading stars at
    * the position buffPos. The function returns the buffer position
    * at which the line 'ENDMDL' ends. */
   size_t ReadModel(ifstream &ifil,const size_t buffPos=0);
   void Save(const string &onam) const;
   void WriteModel(ofstream &ofil) const;
   void DisplayProperties();
   void ParseToScreen();
   void ExtractAtoms();
   void ReloadAtoms();
   static bool RecordTypeIs(const string &line,const string &testrectp);
   /** Copies the atom coordinates contained in line  */
   static void CopyAtomCoordinates(const string &line,vector<double> &x);
   /** Copies the current atoms coordinates to the respective buffer items */
   void CopyAllAtomCoordinates();
   vector<string> buffer;
   vector<size_t> atmBuffIdx; /*!< atmBuffIdx[i] is the buffer index associated with atom[i]  */
   vector<PDBRecordType> buffRecType;
   /** Replaces/overwrites the coordinates of atom[idx] in the respective
    * buffered record (buffer[atmBuffIdx[idx]]).  */
   void ReplaceCoordinatesInRecord(const size_t idx,const vector<double> &x);
   void SetVerboseLevel(short int sv) { verboseLevel=sv; }
   PDBRecordType GetRecordType(const string &line);
/* ************************************************************************** */
protected:
   short int verboseLevel;
   bool showHWarnings;
/* ************************************************************************** */
};
/* ************************************************************************** */
std::ostream &operator<<(std::ostream &out,const InputMoleculePDB (&mol));

#endif  /* _INPUTMOLECULE_PDB_H_ */

