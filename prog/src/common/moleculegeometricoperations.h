#ifndef _MOLECULEGEOMETRICOPERATIONS_H_
#define _MOLECULEGEOMETRICOPERATIONS_H_
#include <memory>
using std::shared_ptr;
#include "molecule.h"
#include "moleculeinertiatensor.h"
#include "matrixvectoroperations3d.h"

/* ************************************************************************** */
/** This class (MoleculeGeometricOperations) is basically a container of
 * functions that act upon Molecule objects. The functions should be
 * restricted to geometric operations. 
 * Recall that MoleculeInertiaTensor has an internal pointer to the
 * molecule to which it belongs. */
class MoleculeGeometricOperations {
/* ************************************************************************** */
public:
   /** As the name suggest, this function aligns the first moment
    * of inertia (the greater) along the z-axis, and the
    * second moment of inertia along the y-axis.
    * The atom coordinates of the aligned molecule will be
    * sorted if srt==true */
   static void AlignFirstAndSecondMoments(shared_ptr<Molecule> &mol,\
         shared_ptr<MoleculeInertiaTensor> &I,bool srt=true);
   /** Aligns the molecule using the main moment of inertia. Recall
    * that the jama diagonalization function orders the eigenvalues
    * in an ascending manner. The principal moments of inertia should
    * be positive, thus we are asumming that the principal
    * moment of inertia is the third of the eigenvalues.
    * (There is a message that should warn agains the odd
    * case wherein a negative eigenvalue had been found --see
    * MoleculeInertiaTensor::Diagonalize().)  */
   static void AlignFirstMomentOfInertiaToZ(shared_ptr<Molecule> &mol,\
         shared_ptr<MoleculeInertiaTensor> &I);
   /** This will rotate the molecule in such a manner that  the second
    * moment of inertia is aligned to the x-axis. Notice: this requires
    * that the molecule had already been rotated so as to the 
    * main moment of inertia is aligned to the z-axis.  */
   static void AlignSecondMomemtOfInertaiToY(shared_ptr<Molecule> &mol,\
         shared_ptr<MoleculeInertiaTensor> &I);
   /** Returns the RMSD of the coordinates of two molecules, m1 and m2.
    * Notice: This function assumes that both molecules have been
    * centered and rotated in such a manner that the two first principal
    * moments of inertia are aligned to zx.
    * The atom coordinates of the aligned molecule are assumed to
    * be sorted!*/
   static double RMSD(shared_ptr<Molecule> &m1,shared_ptr<Molecule> &m2);
   /** As the name suggests, this function rotates all atom coordinates
    * by 90 degrees around the z-axis. At the end of the rotation,
    * it also calls the SortCoordinates() method.  */
   static void Rotate90DegAroundZAndSort(shared_ptr<Molecule> &mol);
   static void Rotate90DegAroundYAndSort(shared_ptr<Molecule> &mol);
   static void Rotate180DegAroundYAndSort(shared_ptr<Molecule> &mol);
   static void Rotate90DegAroundXAndSort(shared_ptr<Molecule> &mol);
   static void RotateWithRotationMatrix(shared_ptr<Molecule> &mol,\
         vector<vector<double> > &RR);
   static void InvertCoordinates(shared_ptr<Molecule> &mol);
   static void RotateUsingEulerAngles(shared_ptr<Molecule> &mol,\
         const double alpha,const double beta,const double gamma);
   static void RotateUsingEulerAngles(shared_ptr<Molecule> &mol);
   static void TranslateCoordinates(shared_ptr<Molecule> &mol,const vector<double> &a);
   static void TranslateCoordinates(shared_ptr<Molecule> &mol);
/* ************************************************************************** */
protected:
/* ************************************************************************** */
   static MatrixVectorOperations3D mop;
/* ************************************************************************** */
};
/* ************************************************************************** */


#endif  /* _MOLECULEGEOMETRICOPERATIONS_H_ */

