#include <cstdlib>
#include <iostream>
using std::cout;
#include <fstream>
using std::ifstream;
using std::ofstream;
#include <iomanip>
using std::scientific;
using std::setprecision;
#include <cmath>
#include "opendxvolumetricmap.h"
#include "fileutils.h"
#include "screenutils.h"
#include "stringtools.h"

OpenDxVolumetricMap::OpenDxVolumetricMap() {
   origin.resize(3);
   xaxis.resize(3);
   yaxis.resize(3);
   zaxis.resize(3);
   xnpts=ynpts=znpts=-1;
   imsetup=false;
}
OpenDxVolumetricMap::OpenDxVolumetricMap(const OpenDxVolumetricMap &old) : OpenDxVolumetricMap() {
   for ( size_t i=0 ; i<3 ; ++i ) { origin[i]=old.origin[i]; }
   for ( size_t i=0 ; i<3 ; ++i ) { xaxis[i]=old.xaxis[i]; }
   for ( size_t i=0 ; i<3 ; ++i ) { yaxis[i]=old.yaxis[i]; }
   for ( size_t i=0 ; i<3 ; ++i ) { zaxis[i]=old.zaxis[i]; }
   size_t nn=old.data.size();
   data.resize(nn);
   for ( size_t i=0 ; i<nn ; ++i ) { data[i]=old.data[i]; }
   for ( size_t i=0 ; i<old.comment.size() ; ++i ) { comment.push_back(old.comment[i]); }
   xnpts=old.xnpts; ynpts=old.ynpts; znpts=old.znpts;
   imsetup=true;
}
OpenDxVolumetricMap::OpenDxVolumetricMap(const string &vmfname) : OpenDxVolumetricMap() {
   imsetup=ReadFromFile(vmfname);
}
OpenDxVolumetricMap::~OpenDxVolumetricMap() {
   origin.clear();
   xaxis.clear();
   yaxis.clear();
   zaxis.clear();
   xnpts=ynpts=znpts=-1;
   data.clear();
   imsetup=false;
}
bool OpenDxVolumetricMap::ReadFromFile(const string &vmfname) {
   if ( !(FileUtils::ExtensionMatches(vmfname,"dx")) ) {
      ScreenUtils::DisplayErrorMessage(string("Extension of file ")+vmfname+string(" is not dx!"));
      ScreenUtils::DisplayErrorFileNotOpen(vmfname);
      cout << __FILE__ << ", line: " << __LINE__ << '\n';
      return false;
   }
   ifstream ifil(vmfname);
   if ( !ifil.good() ) {
      ScreenUtils::DisplayErrorFileNotOpen(vmfname);
      cout << __FILE__ << ", line: " << __LINE__ << '\n';
      ifil.close();
      return false;
   }
   string line;
   std::getline(ifil,line);
   while ( line[0]=='#' ) {
      comment.push_back(line);
      std::getline(ifil,line);
   }
   /* Get grid dimensions  */
   string tmpstr="gridpositions counts ";
   size_t found=line.find(tmpstr);
   if ( found==string::npos ) {
      ScreenUtils::DisplayErrorMessage("gridpositions not found!");
      cout << __FILE__ << ", line: " << __LINE__ << '\n';
      ifil.close();
      return false;
   }
   found+=tmpstr.size();
   line=line.substr(found);
   sscanf(line.c_str(),"%d %d %d",&xnpts,&ynpts,&znpts);
   /* Get origin  */
   std::getline(ifil,line);
   tmpstr="origin ";
   found=line.find(tmpstr);
   if ( found==string::npos ) {
      ScreenUtils::DisplayErrorMessage("origin not found!");
      cout << __FILE__ << ", line: " << __LINE__ << '\n';
      ifil.close();
      return false;
   }
   found+=tmpstr.size();
   line=line.substr(found);
   sscanf(line.c_str(),"%lg %lg %lg",&origin[0],&origin[1],&origin[2]);
   /* Get deltas  */
   std::getline(ifil,line);
   tmpstr="delta ";
   found=line.find(tmpstr);
   if ( found==string::npos ) {
      ScreenUtils::DisplayErrorMessage("delta not found!");
      cout << __FILE__ << ", line: " << __LINE__ << '\n';
      ifil.close();
      return false;
   }
   found+=tmpstr.size();
   line=line.substr(found);
   sscanf(line.c_str(),"%lg %lg %lg",&xaxis[0],&xaxis[1],&xaxis[2]);
   std::getline(ifil,line);
   tmpstr="delta ";
   found=line.find(tmpstr);
   if ( found==string::npos ) {
      ScreenUtils::DisplayErrorMessage("delta not found!");
      cout << __FILE__ << ", line: " << __LINE__ << '\n';
      ifil.close();
      return false;
   }
   found+=tmpstr.size();
   line=line.substr(found);
   sscanf(line.c_str(),"%lg %lg %lg",&yaxis[0],&yaxis[1],&yaxis[2]);
   std::getline(ifil,line);
   tmpstr="delta ";
   found=line.find(tmpstr);
   if ( found==string::npos ) {
      ScreenUtils::DisplayErrorMessage("delta not found!");
      cout << __FILE__ << ", line: " << __LINE__ << '\n';
      ifil.close();
      return false;
   }
   found+=tmpstr.size();
   line=line.substr(found);
   sscanf(line.c_str(),"%lg %lg %lg",&zaxis[0],&zaxis[1],&zaxis[2]);
   /* Skip gridconnections line  */
   std::getline(ifil,line);
   tmpstr="gridconnections counts ";
   found=line.find(tmpstr);
   if ( found==string::npos ) {
      ScreenUtils::DisplayErrorMessage("gridconnections counts not found!");
      cout << __FILE__ << ", line: " << __LINE__ << '\n';
      ifil.close();
      return false;
   }
   /* Check array dimensions  */
   std::getline(ifil,line);
   tmpstr="array type double rank ";
   found=line.find(tmpstr);
   if ( found==string::npos ) {
      ScreenUtils::DisplayErrorMessage("array type double rank not found!");
      cout << __FILE__ << ", line: " << __LINE__ << '\n';
      ifil.close();
      return false;
   }
   tmpstr="items ";
   found=line.find(tmpstr);
   line=line.substr(found);
   int gs;
   sscanf(line.c_str(),"items %d data folows",&gs);
   if ( gs!=(xnpts*ynpts*znpts) ) {
      ScreenUtils::DisplayErrorMessage("Inconsistent grid size!");
      cout << __FILE__ << ", line: " << __LINE__ << '\n';
      ifil.close();
      return false;
   }
   data.resize(gs);
   for ( int i=0 ; i<gs ; ++i ) { ifil >> data[i]; }
   std::getline(ifil,line);
   std::getline(ifil,line);
   //cout << line << '\n';
   ifil.close();
   return true;
}
void OpenDxVolumetricMap::Write(const string &outname,bool useDoublePrecision) {
   ofstream ofil(outname);
   if ( !ofil.good() ) {
      ScreenUtils::DisplayErrorFileNotOpen(outname);
      cout << __FILE__ << ", line: " << __LINE__ << '\n';
      ofil.close();
      return;
   }
   for ( size_t i=0 ; i<comment.size() ; ++i ) { ofil << comment[i] << '\n'; }
   ofil << "object 1 class gridpositions counts "
        << xnpts << ' ' << ynpts << ' ' << znpts << '\n';
   ofil << "origin " << origin[0] << ' ' << origin[1] << ' ' << origin[2] << '\n';
   ofil << "delta " << xaxis[0] << ' ' << xaxis[1] << ' ' << xaxis[2] << '\n';
   ofil << "delta " << yaxis[0] << ' ' << yaxis[1] << ' ' << yaxis[2] << '\n';
   ofil << "delta " << zaxis[0] << ' ' << zaxis[1] << ' ' << zaxis[2] << '\n';
   ofil << "object 2 class gridconnections counts " << xnpts << ' ' << ynpts << ' ' << znpts << '\n';
   ofil << "object 3 class array type double rank 0 items " << (xnpts*ynpts*znpts)
        << " data follows\n";
   size_t nn=data.size();
   size_t mm=nn-(nn%3);
   if ( useDoublePrecision ) { ofil << scientific << setprecision(12); }
   for ( size_t i=0 ; i<mm ; ++i ) {
      ofil << data[i];
      if ( i%3==2 ) { ofil << '\n'; } else { ofil << ' '; }
   }
   size_t kk=nn-mm;
   if ( kk==1 ) {
      ofil << data[mm] << '\n';
   } else if ( kk==2 ) {
      ofil << data[mm] << ' ' << data[mm+1] << '\n';
   }
   ofil << "attribute \"dep\" string \"positions\"\n";
   string line=comment[0].substr(1);
   StringTools::RemoveSpacesLeftAndRight(line);
   ofil << "object \"" << line << "\" class field\n";
   ofil << "component \"positions\" value 1\n";
   ofil << "component \"connections\" value 2\n";
   ofil << "component \"data\" value 3\n";
   ofil.close();
}
void OpenDxVolumetricMap::ExportAsDataFile(const string &datname,const string fmt) {
   bool tsvfmt=(fmt==string("tsv"));
   ofstream ofil(datname);
   if ( !ofil.good() ) {
      ScreenUtils::DisplayErrorFileNotOpen(datname);
      cout << __FILE__ << ", line: " << __LINE__ << '\n';
      ofil.close();
      return;
   }
   if ( (xaxis[1] != 0.0) || (xaxis[2] != 0.0) ||
        (yaxis[0] != 0.0) || (yaxis[2] != 0.0) ||
        (zaxis[0] != 0.0) || (zaxis[1] != 0.0) ) {
      ScreenUtils::DisplayErrorMessage("Dx with non orthogonal deltas are not implemented!");
      cout << __FILE__ << ", line: " << __LINE__ << '\n';
      ofil << "#Non orthogonal deltas are not supported in the current\n"
           << "#version of OpenDxVolumetricMap." << '\n'
           << "#Message from: " << __FILE__ << ", line: " << __LINE__ << '\n';
      ofil.close();
      return;
   }
   double x0=origin[0], y0=origin[1], z0=origin[2];
   double x=x0,y=y0,z=z0;
   double dx=xaxis[0],dy=yaxis[1],dz=zaxis[2];
   ofil << "# Data generated by OpenDxVolumetricMap, from a dx file." << '\n';
   ofil << "# Grid size: " << xnpts << ' ' << ynpts << ' ' << znpts << '\n';
   ofil << "# Origin: " << x0 << ' ' << y0 << ' ' << z0 << '\n';
   ofil << "# EndPoint: " << (x0+double(xnpts-1)*dx) << ' '
        << (y0+double(ynpts-1)*dy) << ' ' << (z0+double(znpts-1)*dz) << '\n';
   ofil << "# Deltas (dx dy dz): " << dx << ' ' << dy << ' ' << dz << '\n';
   for ( size_t i=0 ; i<comment.size() ; ++i ) { ofil << comment[i] << '\n'; }
   size_t pos=0;
   for ( int i=0 ; i<xnpts ; ++i ) {
      y=y0;
      for ( int j=0 ; j<ynpts ; ++j ) {
         z=z0;
         for ( int k=0 ; k<znpts ; ++k ) {
            ofil << x << '\t' << y << '\t' << z << '\t' << data[pos++] << '\n';
            z+=dz;
         }
         if (tsvfmt) { ofil << '\n'; }
         y+=dy;
      }
      x+=dx;
   }
   ofil.close();
}
void OpenDxVolumetricMap::Add(const double a) {
   if ( !imsetup ) {
      ScreenUtils::DisplayErrorMessage("OpenDxVolumetricMap not setup!");
      cout << __FILE__ << ", line: " << __LINE__ << '\n';
      return;
   }
   size_t nn=data.size();
   for ( size_t i=0 ; i<nn ; ++i ) { data[i]+=a; }
}
void OpenDxVolumetricMap::Scale(const double a) {
   if ( !imsetup ) {
      ScreenUtils::DisplayErrorMessage("OpenDxVolumetricMap not setup!");
      cout << __FILE__ << ", line: " << __LINE__ << '\n';
      return;
   }
   size_t nn=data.size();
   for ( size_t i=0 ; i<nn ; ++i ) { data[i]*=a; }
}
bool OpenDxVolumetricMap::IsCompatible(const OpenDxVolumetricMap &other) {
   bool res=true;
   res=res&&((data.size())==(other.data.size()));
   res=res&&(xnpts==(other.xnpts));
   res=res&&(ynpts==(other.ynpts));
   res=res&&(znpts==(other.znpts));
   return res;
}
void OpenDxVolumetricMap::Add(const OpenDxVolumetricMap &other) {
   if ( !IsCompatible(other) ) {
      ScreenUtils::DisplayErrorMessage("Cannot add OpenDxVolumetricMap!");
      cout << __FILE__ << ", line: " << __LINE__ << '\n';
      return;
   }
   size_t nn=data.size();
   for ( size_t i=0 ; i<nn ; ++i ) {
      data[i]+=(other.data[i]);
   }
}
void OpenDxVolumetricMap::AddPowerOfOther(const OpenDxVolumetricMap &other,const int kk) {
   if ( !IsCompatible(other) ) {
      ScreenUtils::DisplayErrorMessage("Cannot add OpenDxVolumetricMap!");
      cout << __FILE__ << ", line: " << __LINE__ << '\n';
      return;
   }
   if ( kk<2 ) {
      ScreenUtils::DisplayErrorMessage("In this version, only integers > 1 are allowed on\n"
            "AddPowerOfOther function! Nothing done on the dxmap.");
      cout << __FILE__ << ", line: " << __LINE__ << '\n';
      return;
   }
   size_t nn=data.size();
   double tmp0,tmpK;
   for ( size_t i=0 ; i<nn ; ++i ) {
      tmp0=other.data[i];
      tmpK=tmp0;
      for ( int j=1 ; j<kk ; ++j ) { tmpK*=tmp0; }
      data[i]+=tmpK;
   }
}
void OpenDxVolumetricMap::Substract(const OpenDxVolumetricMap &other) {
   if ( !IsCompatible(other) ) {
      ScreenUtils::DisplayErrorMessage("Cannot add OpenDxVolumetricMap!");
      cout << __FILE__ << ", line: " << __LINE__ << '\n';
      return;
   }
   size_t nn=data.size();
   for ( size_t i=0 ; i<nn ; ++i ) {
      data[i]-=(other.data[i]);
   }
}
void OpenDxVolumetricMap::Power(const int kk) {
   if ( !imsetup ) {
      ScreenUtils::DisplayErrorMessage("OpenDxVolumetricMap not setup!");
      cout << __FILE__ << ", line: " << __LINE__ << '\n';
      return;
   }
   if ( kk<1 ) {
      ScreenUtils::DisplayErrorMessage("In this version, only positive integers are allowed on\n"
            "Power function! Nothing done on the dxmap.");
      cout << __FILE__ << ", line: " << __LINE__ << '\n';
      return;
   }
   size_t nn=data.size();
   double tmp0,tmpK;
   for ( size_t i=0 ; i<nn ; ++i ) {
      tmp0=data[i];
      tmpK=tmp0;
      for ( int j=1 ; j<kk ; ++j ) { tmpK*=tmp0; }
      data[i]=tmpK;
   }
}
void OpenDxVolumetricMap::SqrRoot() {
   size_t nn=data.size();
   for ( size_t i=0 ; i<nn ; ++i ) { data[i]=sqrt(data[i]); }
}
void OpenDxVolumetricMap::Zeros() {
   size_t nn=data.size();
   for ( size_t i=0 ; i<nn ; ++i ) { data[i]=0.0e0; }
}

