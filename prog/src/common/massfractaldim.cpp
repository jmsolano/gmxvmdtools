#include <cstdlib>
#include <iostream>
using std::cout;
#include <vector>
using std::vector;
#include <fstream>
using std::ofstream;
#include <iomanip>
using std::scientific;
using std::setprecision;
#include <cmath>
#include <chrono>
#include "screenutils.h"
#include "massfractaldim.h"
//The default number of radii \f$r_i,\ i=1,\ \dots\ ,\ N_r\f$:
#define DEFAULT_N_RADII 20
// The number of measures used to compute each \f$\bar N_i\f$
#define DEFAULT_N_MEASURING 30
// The minimum radius of the circles around each measuring center.
#define DEFAULT_MEAS_R_MIN 0.5e0
// The default fraction of the cluster that will be considered for computing averages.
#define DEFAULT_CLUSTER_FRACTION 0.75e0
// The default fraction to determine the maximum measuring circle
#define DEFAULT_MEAS_R_MAX_FRAC 0.15

std::mt19937_64 MassFractalDim::generator;
std::uniform_int_distribution<size_t> MassFractalDim::distrib(0,100);
MassFractalDim::MassFractalDim() {
   r.clear();
   N.clear();
   M.clear();
   E.clear();
   mol=nullptr;
   x.clear();
   mass.clear();
   numel.clear();
   nat=0;
   Rmax=seekRmax=mrmax=-1.0e+50;
   mrmin=DEFAULT_MEAS_R_MIN;
   dr=-1.0e+50;
   nr=DEFAULT_N_RADII;
   nMeas=DEFAULT_N_MEASURING;
   nw=128;
   pw.clear();
   generator.seed(size_t(std::time(nullptr)));
   imsetup=false;
}
MassFractalDim::~MassFractalDim() {
   r.clear();
   N.clear();
   M.clear();
   E.clear();
   for ( size_t i=0 ; i<x.size() ; ++i ) { x[i].clear(); }
   x.clear();
   mass.clear();
   numel.clear();
}
MassFractalDim::MassFractalDim(shared_ptr<Molecule> umol)
   : MassFractalDim() {
   mol=umol;
   mol->CenterAtCentroid();
   nat=mol->Size();
   distrib=std::uniform_int_distribution<size_t>(0,nat-1);
   imsetup=SetupInternalVariables();
   mol->ResetOriginOfCoordinates();
}
void MassFractalDim::SetNumberOfMeasuringCircles(const int un) {
   nr=size_t(un);
   r.resize(nr);
   N.resize(nr);
   M.resize(nr);
   E.resize(nr);
   imsetup=SetupInternalVariables();
}
bool MassFractalDim::SetupInternalVariables() {
   bool res=true;
   if ( nat==0 ) { return false; }
   if ( x.size() != nat ) {
      for ( size_t i=0 ; i<x.size() ; ++i ) { x[i].clear(); }
      x.resize(nat);
      for ( size_t i=0 ; i<nat ; ++i ) { x[i].resize(3); }
      for ( size_t i=0 ; i<nat ; ++i ) {
         x[i][0]=mol->atom[i].x[0];
         x[i][1]=mol->atom[i].x[1];
         x[i][2]=mol->atom[i].x[2];
      }
   }
   if ( mass.size() != nat ) { mass.resize(nat,1.0e0); }
   for ( size_t i=0 ; i<nat ; ++i ) { mass[i]=mol->atom[i].weight; }
   if ( numel.size() != nat ) { numel.resize(nat,1.0e0); }
   for ( size_t i=0 ; i<nat ; ++i ) { numel[i]=double(mol->atom[i].num); }
   double rt,lclrmx=-1.0e+50;
   for ( size_t i=0 ; i<nat ; ++i ) {
      rt=0.0e0;
      rt+=((x[i][0])*(x[i][0]));
      rt+=((x[i][1])*(x[i][1]));
      rt+=((x[i][2])*(x[i][2]));
      if ( rt>lclrmx ) { lclrmx=rt; }
   }
   if ( lclrmx<0.0e0 ) { return false; }
   Rmax=sqrt(lclrmx);
   if ( seekRmax<0.0e0 ) { seekRmax=(DEFAULT_CLUSTER_FRACTION)*Rmax; }
   if ( mrmax<0.0e0 ) { mrmax=DEFAULT_MEAS_R_MAX_FRAC*Rmax; }
   if ( nr<1 ) { return false; }
   dr=(mrmax-mrmin)/double(nr-1);
   if ( r.size() != nr ) {
      r.resize(nr);
      N.resize(nr);
      M.resize(nr);
      E.resize(nr);
   }
   double rho=3.0e0*double(nat)/(4.0e0*M_PI*Rmax*Rmax*Rmax);
   nw=32*(size_t(2.0e0*rho*4.0e0*M_PI*mrmax*mrmax*mrmax/3.0e0)/32+1);
   pw.clear();
   pw.reserve(nw);
   return res;
}
void MassFractalDim::DisplayProperties() {
   if ( x.size() != nat ) {
      ScreenUtils::DisplayErrorMessage("Number of atoms and internal array x size do not match!");
      cout << __FILE__ << ", fnc: " << __FUNCTION__ << ", line: " << __LINE__ << '\n';
   }
   cout << "     nat: " << nat << '\n';
   cout << "    Rmax: " << Rmax << '\n';
   cout << "seekRmax: " << seekRmax << '\n';
   cout << "    Nris: " << r.size() << '\n';
   cout << "   mrmax: " << mrmax << '\n';
   cout << "   mrmin: " << mrmin << '\n';
   cout << "      dr: " << dr << '\n';
   cout << "   nMeas: " << nMeas << '\n';
}
void MassFractalDim::DisplayResults() {
   cout << "# r    N     M     E" << '\n';
   for ( size_t i=0 ; i<nr ; ++i ) {
      cout << r[i] << " " << N[i] << ' ' << M[i] << ' ' << E[i] << '\n';
   }
}
void MassFractalDim::WriteResults(const string &outfname) {
   ofstream ofil(outfname);
   if ( !(ofil.good()) ) {
      ScreenUtils::DisplayErrorFileNotOpen(outfname);
      cout << __FILE__ << ", fnc: " << __FUNCTION__ << ", line: " << __LINE__ << '\n';
      ofil.close();
      return;
   }
   ofil << "#  r      N_{at}(r)   M(r)     N_{el}(r)" << '\n';
   ofil << scientific << setprecision(12);
   for ( size_t i=0 ; i<nr ; ++i ) {
      ofil << r[i] << ' ' << N[i] << ' ' << M[i] << ' ' << E[i] << '\n';
   }
   ofil.close();
}
void MassFractalDim::ComputeNvsR() {
   if ( !imsetup ) {
      ScreenUtils::DisplayErrorMessage("MassFractalDim object not properly setup!");
      cout << __FILE__ << ", fnc: " << __FUNCTION__ << ", line: " << __LINE__ << '\n';
      return;
   }
   InitVectors();
   size_t idx;
   //cout << "Cap: " << pw.capacity() << ", size: " << pw.size() << '\n';
   for ( size_t i=0 ; i<nMeas ; ++i ) {
      idx=GetSeedIdx();
      AddSingleCount(idx);
   }
   for ( size_t i=0 ; i<nr ; ++i ) { N[i]/=double(nMeas); }
   for ( size_t i=0 ; i<nr ; ++i ) { M[i]/=double(nMeas); }
   for ( size_t i=0 ; i<nr ; ++i ) { E[i]/=double(nMeas); }
}
void MassFractalDim::AddSingleCount(const size_t idx) {
   size_t count,narr,tid;
   double rc[3];
   rc[0]=x[idx][0]; rc[1]=x[idx][1]; rc[2]=x[idx][2];
   double rr,rtmp,mtmp,eltmp;
   IdentifyParticlesInsideMRmaxCircle(idx);
   narr=pw.size();
   for ( size_t i=0 ; i<nr ; ++i ) {
      rtmp=r[i]*r[i];
      mtmp=eltmp=0.0e0;
      count=0;
      for ( size_t j=0 ; j<narr ; ++j ) {
         tid=pw[j];
         rr=0.0e0;
         rr+=((x[tid][0]-rc[0])*(x[tid][0]-rc[0]));
         rr+=((x[tid][1]-rc[1])*(x[tid][1]-rc[1]));
         rr+=((x[tid][2]-rc[2])*(x[tid][2]-rc[2]));
         if ( rr<=rtmp ) {
            ++count;
            mtmp+=mass[tid];
            eltmp+=numel[tid];
         }
      }
      N[i]+=double(count);
      M[i]+=mtmp;
      E[i]+=eltmp;
   }
   //cout << "Parts inside mrmax: " << pw.size() << '\n';
   //cout << "Cap: " << pw.capacity() << ", size: " << pw.size() << '\n';
}
void MassFractalDim::IdentifyParticlesInsideMRmaxCircle(const size_t idx) {
   pw.clear();
   double xc[3];
   xc[0]=x[idx][0]; xc[1]=x[idx][1]; xc[2]=x[idx][2];
   double rr,r2=mrmax*mrmax;
   for ( size_t i=0 ; i<nat ; ++i ) {
      rr=0.0e0;
      rr+=((x[i][0]-xc[0])*(x[i][0]-xc[0]));
      rr+=((x[i][1]-xc[1])*(x[i][1]-xc[1]));
      rr+=((x[i][2]-xc[2])*(x[i][2]-xc[2]));
      if ( rr<r2 ) { pw.push_back(i); }
   }
}
size_t MassFractalDim::GetSeedIdx() {
   size_t idx=distrib(generator);
   size_t count=0;
   double rc=(x[idx][0]*x[idx][0])+(x[idx][1]*x[idx][1])+(x[idx][2]*x[idx][2]);
   while ( ((sqrt(rc)+mrmax)>seekRmax) && (count<10000) ) {
      idx=distrib(generator);
      rc=(x[idx][0]*x[idx][0])+(x[idx][1]*x[idx][1])+(x[idx][2]*x[idx][2]);
      ++count;
   }
   if ( count==10000 ) {
      ScreenUtils::DisplayErrorMessage("Could not find a suitable seed within 10000 attempts!");
      cout << __FILE__ << ", fnc: " << __FUNCTION__ << ", line: " << __LINE__ << '\n';
      return string::npos;
   }
   return idx;
}
void MassFractalDim::InitVectors() {
   if ( !imsetup ) {
      ScreenUtils::DisplayErrorMessage("Object of class MassFractalDim not propperly initialized!");
      cout << __FILE__ << ", fnc: " << __FUNCTION__ << ", line: " << __LINE__ << '\n';
      return;
   }
   for ( size_t i=0 ; i<nr ; ++i ) { r[i]=mrmin+(double(i)*dr); }
   for ( size_t i=0 ; i<nr ; ++i ) { N[i]=0.0e0; }
   for ( size_t i=0 ; i<nr ; ++i ) { M[i]=0.0e0; }
   for ( size_t i=0 ; i<nr ; ++i ) { E[i]=0.0e0; }
}

