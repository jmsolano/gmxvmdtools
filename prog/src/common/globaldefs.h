//Here you can define the macros that are used WITHIN
//this directory. Notice that the macros defined here
//affect ALL and every single *cc and *cpp source.
#ifndef _GLOBALDEFS_H_
#define _GLOBALDEFS_H_

#define DISPLAYDEBUGINFOFILELINE (std::cout << __FILE__ << ", line: " << __LINE__ << std::endl)

#define CURRENTVERSION "1.0.0"

#endif /* _GLOBALDEFS_H_ */
