#include <cstdlib>
#include <iostream>
using std::cout;
#include <iomanip>
using std::setprecision;
using std::setw;
#include <fstream>
using std::ifstream;
using std::ofstream;
#include "pdbtools.h"
#include "stringtools.h"
#include "fileutils.h"

PDBTools::PDBTools() {
}
void PDBTools::Translate(const string &inname,const vector<double> &t) {
   string ofname=inname;
   FileUtils::InsertAtEndOfFileName(ofname,"Trans");
   return Translate(inname,t,ofname);
}
void PDBTools::Translate(const string &inname,const vector<double> &t,\
      string &outname) {
   ifstream ifil(inname);
   ofstream ofil(outname);
   string str;
   string kwd="ATOM";
   vector<double> x(3);
   while ( !ifil.eof() ) {
      getline(ifil,str);
      if ( RecordTypeIs(str,kwd) ) {
         CopyAtomCoordinates(str,x);
         for ( size_t i=0 ; i<3 ; ++i ) { x[i]+=t[i]; }
         ofil << std::fixed << setprecision(3);
         ofil << str.substr(0,30);
         ofil << setw(8) << x[0] << setw(8) << x[1] << setw(8) << x[2];
         ofil << str.substr(54,string::npos) << '\n';
      } else {
         if ( str.size()!=0 ) { ofil << str << '\n'; }
      }
   }
   ifil.close();
   ofil.close();
}
bool PDBTools::RecordTypeIs(const string &line,const string &testrectp) {
   size_t n=testrectp.size();
   if ( n>line.size() ) { return false; }
   return line.substr(0,n)==testrectp;
}
void PDBTools::CopyAtomCoordinates(const string &line,vector<double> &x) {
   /* See: http://www.wwpdb.org/documentation/file-format-content/format33/sect9.html#ATOM  */
   x[0]=std::stod(line.substr(31,8));
   x[1]=std::stod(line.substr(39,8));
   x[2]=std::stod(line.substr(47,8));
}

