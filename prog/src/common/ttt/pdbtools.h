#ifndef _PDBTOOLS_H_
#define _PDBTOOLS_H_
#include <string>
using std::string;
#include <vector>
using std::vector;

/* ************************************************************************** */
class PDBTools {
/* ************************************************************************** */
public:
/* ************************************************************************** */
   PDBTools();
   static void Translate(const string &inname,const vector<double> &t);
   static void Translate(const string &inname,const vector<double> &t,string &outname);
   static bool RecordTypeIs(const string &line,const string &testrectp);
   static void CopyAtomCoordinates(const string &line,vector<double> &x);
/* ************************************************************************** */
protected:
/* ************************************************************************** */
/* ************************************************************************** */
};
/* ************************************************************************** */


#endif  /* _PDBTOOLS_H_ */

