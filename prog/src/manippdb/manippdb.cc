#include <cstdlib>
#include <iostream>
using std::cout;
using std::endl;
using std::cerr;
#include <memory>
using std::shared_ptr;
#include <iomanip>
using std::scientific;
using std::setprecision;
#include <string>
using std::string;
#include <memory>
using std::shared_ptr;
#include <fstream>
using std::ifstream;
using std::ofstream;
#include "optflags.h"
#include "screenutils.h"
#include "fileutils.h"
#include "mytimer.h"
#include "inputmolecule_pdb.h"
#include "moleculegeometricoperations.h"
#include "moleculeinertiatensor.h"

int main (int argc, char *argv[]) {
   /* ************************************************************************** */
   MyTimer timer;
   timer.Start();
   /* ************************************************************************** */
   /* Configures the program (options, variables, etc.)  */
   shared_ptr<OptionFlags> options = shared_ptr<OptionFlags>(new OptionFlags(argc,argv));
   switch ( options->GetExitCode() ) {
      case OptionFlagsBase::ExitCode::OFEC_EXITNOERR :
         std::exit(EXIT_SUCCESS);
         break;
      case OptionFlagsBase::ExitCode::OFEC_EXITERR :
         std::exit(EXIT_FAILURE);
         break;
      default :
         break;
   }
   string inname=string(argv[1]);
   string outname=inname;
   FileUtils::InsertAtEndOfFileName(outname,"Mod");
   ScreenUtils::PrintHappyStart(argv,CURRENTVERSION,PROGRAMCONTRIBUTORS);

   /* Main corpus  */

   int count=1;
   cout << "Reading frame " << (count++) << '\n' << std::flush;
   shared_ptr<InputMoleculePDB> pdbmol=
      shared_ptr<InputMoleculePDB>(new InputMoleculePDB());
   ifstream ifil(inname);
   size_t pos=pdbmol->ReadModel(ifil);
   shared_ptr<Molecule> mol=shared_ptr<Molecule>( pdbmol );
   cout << mol->cm[0] << ' ' << mol->cm[1] << ' ' << mol->cm[2] << '\n';
   cout << mol->cd[0] << ' ' << mol->cd[1] << ' ' << mol->cd[2] << '\n';
   shared_ptr<MoleculeInertiaTensor> inertia=
      std::make_shared<MoleculeInertiaTensor>(mol);
   MoleculeGeometricOperations::AlignFirstAndSecondMoments(mol,inertia,false);
   //pdbmol->ComputeCenterOfMass();
   //pdbmol->ComputeCentroid();
   inertia->DisplayEigenvalues();
   inertia->DisplayEigenvectors();
   pdbmol->ResetOriginOfCoordinates();
   pdbmol->CopyAllAtomCoordinates();
   ofstream ofil(outname);
   pdbmol->WriteModel(ofil);
   while ( pos!=string::npos ) {
      cout << "Reading frame " << (count++) << '\n' << std::flush;
      pos=pdbmol->ReadModel(ifil,pos);
      if ( pos!=string::npos ) {
         inertia->Setup();
         MoleculeGeometricOperations::AlignFirstAndSecondMoments(mol,inertia,false);
         inertia->DisplayEigenvalues();
         inertia->DisplayEigenvectors();
         pdbmol->ResetOriginOfCoordinates();
         pdbmol->CopyAllAtomCoordinates();
         //pdbmol->ComputeCenterOfMass();
         //pdbmol->ComputeCentroid();
         pdbmol->WriteModel(ofil);
      }
   }
   cout << '\n';

   if ( options->translate ) {
      vector<double> t(3);
      for ( size_t i=0 ; i<3 ; ++i ) {
         t[i]=std::stod(string(argv[options->translate+i]));
      }
      MoleculeGeometricOperations::TranslateCoordinates(mol,t);
   }

   ifil.close();
   ofil.close();
   /* All OK  */
   ScreenUtils::PrintHappyEnding();
   timer.End();
   timer.PrintElapsedTimeSec(string("global timer"));
   //         1         2         3         4         5         6         7         8
   //123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
   //ATOM      1  N   GLY     1     188.560 239.250  94.670  1.00  0.00
   /* ************************************************************************** */
   return EXIT_SUCCESS;
}

