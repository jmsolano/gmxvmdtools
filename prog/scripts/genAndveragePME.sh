#!/bin/bash
if [[ "$OSTYPE" == "darwin"* ]]; then
   #this only works in Manuel laptop. Change the following path.
   #if yoy have a MacOSX laptop, by the respective vmd complete path
   #VMD="/Applications/Science/VMD1.9.4.app/Contents/vmd/vmd_MACOSXX86"
   VMD="/Applications/Science/VMD1.9.4.app/Contents/vmd/vmd_MACOSXARM64"
   SED="gsed"
else
   VMD="/usr/local/bin/vmd"
   SED="sed"
fi
prog_name=$0

help_and_syntax() {
   the_prog_name=$1
echo -e "\n    Script syntax: \033[1m$the_prog_name \033[0m [\033[0;4moption(s)\033[0m \
[\033[4margument(s)]]\033[0m"
#Here you may put a description of the script. The text between 'END_USAGE_TEXT'
#will be displayed as it is.
cat << END_USAGE_TEXT

This script computes the average of the PME field of a series of pqr files.
The list of pqr files is taken from the ls command (ls *.pqr).
The script will call VMD's PME_pot plugin to compute the PME of the FIRST
pqr in the list, and save it in a temporary filename.
For each subsequent pqr in the list, the script will:
   call VMD's PME_pot
   save the PME in a dx file
   add this PME to the initial PME
   save the added PMEs in the same temporary filename (this overwrites the
      previous information)
Finally, the script scales the total PME contained in the temporary filename
by the total number of pqrs present in the working directory.

Warning: In this version, the above procedure cannot be restarted. However,
if you need it, you can use manipdx to recompute an incomplete calculation.

The available options are:
   -n  npts     Set the grid's number of points per direction. I.e.,
                  the grid (dx file) will be a npts x npts x npts
                  grid.
   -D           Set the debug mode (in case there is information
                      for developers).
   -o outname   Set the final name for the script to be "outname"
                  Default; averagePME.dx
   -v               Set the verbose mode (print additional information)
   -h               Display the help menu.

END_USAGE_TEXT
}

#if [ $# -lt 1 ]
#then
#   echo "Error: input filename missing (Try -h for more information.)."
#   exit 2
#fi

inDxName=$BASH_ARGV
meanDxName="averagePME.dx"
stdDevDxName="stddevPME.dx"
gridNPts="20"
verbMode="false"
debugMode="false"

while getopts "::hDn:vo:O:" opt; do
  case $opt in
    n)
      gridNPts=$OPTARG
      ;;
    o)
      meanDxName=$OPTARG
      ;;
    O)
      stdDevDxName=$OPTARG
      ;;
    D)
      debugMode="true"
      ;;
    h)
      help_and_syntax $prog_name
      exit 0
      ;;
    v)
      verbMode="true"
      ;;
    \?)
      echo "Invalid option: -$OPTARG" >&2
      echo -e "(Try\n   $prog_name -h\nfor more information.)"
      exit 1
      ;;
    :)
      echo "Option -$OPTARG requires an argument." >&2
      echo -e "(Try\n   $prog_name -h\nfor more information.)"
      exit 1
      ;;
  esac
done

#requires 1 argument: pqr_name
get_a_minus_b_vector() {
   the_a_vec="$1"
   the_b_vec="$2"
   aa=($the_a_vec)
   bb=($the_b_vec)
   dd=$(echo "${aa[0]} - ${bb[0]}" | bc -l)
   dd="$dd $(echo "${aa[1]} - ${bb[1]}" | bc -l)"
   dd="$dd $(echo "${aa[2]} - ${bb[2]}" | bc -l)"
   echo "$dd"
}
get_a_plus_b_vector() {
   the_a_vec="$1"
   the_b_vec="$2"
   aa=($the_a_vec)
   bb=($the_b_vec)
   dd=$(echo "${aa[0]} + ${bb[0]}" | bc -l)
   dd="$dd $(echo "${aa[1]} + ${bb[1]}" | bc -l)"
   dd="$dd $(echo "${aa[2]} + ${bb[2]}" | bc -l)"
   echo "$dd"
}
get_geometric_center() {
   the_pwd="$(pwd)"
   the_pqr="$the_pwd/$1"
   the_vmd_script="${RANDOM}${RANDOM}.vmd"
   echo "mol new {$the_pqr} type {pqr} first 0 last -1 step 1 waitfor 1" > $the_vmd_script
   echo "set all [atomselect top \"all\"]" >> $the_vmd_script
   echo "puts \"LMMSSDGeomCentBeg\"" >> $the_vmd_script
   echo -e "measure center \$all" >> $the_vmd_script
   echo "puts \"LMMSSDGeomCentEnd\"" >> $the_vmd_script
   echo "quit" >> $the_vmd_script
   $VMD -dispdev none -e $the_vmd_script | \
      $SED -n '/LMMSSDGeomCentBeg/,/LMMSSDGeomCentEnd/{/LMMSSDGeomCentBeg/!{/LMMSSDGeomCentEnd/!p}}'
   rm -f $the_vmd_script
}
gen_reference_dx_file() {
   the_pwd="$(pwd)"
   the_pqr="$the_pwd/$1"
   the_dx_filename="${the_pqr%pqr}dx"
   the_grid_pts="$2"
   the_vmd_log_file="$3"
   the_vmd_script="${RANDOM}${RANDOM}.vmd"
   echo "mol new {$the_pqr} type {pqr} first 0 last -1 step 1 waitfor 1" > $the_vmd_script
   echo "package require pmepot" >> $the_vmd_script
   echo -e "set all [atomselect top \"all\"]" >> $the_vmd_script
   echo -e "pmepot -sel \$all -frames now -grid {$the_grid_pts $the_grid_pts $the_grid_pts} -ewaldfactor 0.25 -dxfile $the_dx_filename" >> $the_vmd_script
   echo "quit" >> $the_vmd_script
   $VMD -dispdev none -e $the_vmd_script | grep -e "cell:" | $SED -e 's/cell: //'  > $the_vmd_log_file
   rm $the_vmd_script
}
#Requires 3 arguments: raw_pqr_name, grid_n_pts, vecDr vecsABCcellFmt
gen_dx_file() {
   the_raw_pqr="$1"
   the_pwd="$(pwd)"
   the_pqr="$the_pwd/$the_raw_pqr"
   the_dx_filename="${the_pqr%pqr}dx"
   the_grid_pts="$2"
   the_dd_vec="$3"
   the_abc_vecs="$4"
   the_gg_vec="$(get_geometric_center $the_raw_pqr)"
   the_oo_vec="$(get_a_plus_b_vector "$the_gg_vec" "$the_dd_vec")"
   the_cell_specs="$(echo "{$the_oo_vec} $the_abc_vecs")"
   the_vmd_script="${RANDOM}${RANDOM}.vmd"
   echo "mol new {$the_pqr} type {pqr} first 0 last -1 step 1 waitfor 1" > $the_vmd_script
   echo "package require pmepot" >> $the_vmd_script
   echo -e "set all [atomselect top \"all\"]" >> $the_vmd_script
   the_pmepot_args="-sel \$all -frames now -grid {$the_grid_pts $the_grid_pts $the_grid_pts}"
   the_pmepot_args="$the_pmepot_args -cell {$the_cell_specs}"
   the_pmepot_args="$the_pmepot_args -ewaldfactor 0.25 -dxfile $the_dx_filename"
   echo -e "pmepot $the_pmepot_args" >> $the_vmd_script
   echo "quit" >> $the_vmd_script
   #$VMD -dispdev none -e $the_vmd_script | grep -e "cell:" | $SED -e 's/cell: //'
   $VMD -dispdev none -e $the_vmd_script > /dev/null
   rm $the_vmd_script
}
get_reference_origin_vector() {
   the_log_vmd_file="$1"
   cat $the_log_vmd_file | tr -d '{' | tr -d '}' | tr -s ' ' | awk '{print $1" "$2" "$3}'
}
get_reference_Kth_vector() {
   the_log_vmd_file="$1"
   the_k_idx=$2
   cat $the_log_vmd_file | $SED -e 's/} {/}\n{/g' | $SED -n ${the_k_idx}p | tr -d '{' | tr -d '}'
}
compose_cell_spec_no_orig() {
   the_A="$1"
   the_B="$2"
   the_C="$3"
   echo "{$the_A} {$the_B} {$the_C}"
}

#The corpus of the script
shift $(($OPTIND - 1))
if [ "$debugMode" == "true" ];then
   echo "sed: $SED"
   echo "vmd: $VMD"
fi

#Some help to the developer:
#Get array from ls: theArray=($(ls -1 *.txt 2>/dev/null | tr '\n' ' '))
#the number of items in an array: nTerms=$(( ${#theArray[@]} - 1 ))
#Get the i entry of an array: echo "${theArray[i]}"
#the number of items in a list: nTerms=$( echo $list | wc -w)
arrayPqrs=($(ls -1 *.pqr 2>/dev/null | tr '\n' ' '))
nTerms=$(( ${#arrayPqrs[@]} - 1 ))
recipNTerms=$(echo "1.0 / ( $nTerms.0 + 1.0 )" | bc -l)

refPQR="${arrayPqrs[0]}"
refDX="${arrayPqrs[0]%pqr}dx"
vmdCellRefFile="vmdcellrefframe.log"

shift $(($OPTIND - 1))

echo "Processing reference pqr: $refPQR..."
lstGr="$(get_geometric_center $refPQR)"
gen_reference_dx_file $refPQR $gridNPts $vmdCellRefFile
lstOr=$(get_reference_Kth_vector $vmdCellRefFile 1)
lstAr="$(get_reference_Kth_vector $vmdCellRefFile 2)"
lstBr="$(get_reference_Kth_vector $vmdCellRefFile 3)"
lstCr="$(get_reference_Kth_vector $vmdCellRefFile 4)"
lstDr="$(get_a_minus_b_vector "$lstOr" "$lstGr")"
cellSpecsNoOrig="$(compose_cell_spec_no_orig "$lstAr" "$lstBr" "$lstCr")"

if [ "$debugMode" == "true" ];then
   echo Or: $lstOr
   echo Gr: $lstGr
   echo Dr: $lstDr
   echo Ar: $lstAr
   echo Br: $lstBr
   echo Cr: $lstCr
   echo "refDX: $refDX"
   echo "1/n: $recipNTerms"
fi
#for i in $(seq 0 2);do
#   echo ${vecOr[$i]}
#done
cp $refDX $stdDevDxName
cp $refDX $meanDxName
manipdx $stdDevDxName -p 2 -O -q
for i in $(seq 1 $nTerms);do
   lcl_pqr="${arrayPqrs[$i]}"
   lcl_dx="${lcl_pqr%pqr}dx"
   echo "Processing $lcl_pqr..."
   gen_dx_file $lcl_pqr $gridNPts "$lstDr" "$cellSpecsNoOrig"
   manipdx $meanDxName -A $lcl_dx -O -q -d
   manipdx $stdDevDxName --add-pow-other $lcl_dx 2 -O -q -d
   rm $lcl_dx
done

manipdx $meanDxName -m $recipNTerms -O -q

tmpDxName="${RANDOM}${RANDOM}.dx"
#tmpDxName="tmpbdebug.dx"
cp $meanDxName $tmpDxName
manipdx $tmpDxName -p 2 -O -q
manipdx $stdDevDxName -m $recipNTerms -O -q
manipdx $stdDevDxName -S $tmpDxName -O -q
manipdx $stdDevDxName -r -O -q

#head $tmpDxName
rm $tmpDxName


